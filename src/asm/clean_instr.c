/*
** clean_instr.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:33:11 2014 alexis abra
** Last update Sun Apr 13 18:59:28 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"

static char	*space_less(char *str)
{
  int		i;

  i = 0;
  while (str[i++]);
  if (str[i - 2] == ' ' || str[i - 2] == '\t')
    {
      while (str[i - 2] == ' '|| str[i - 2] == '\t')
	i--;
      str[i - 1] = '\0';
    }
  return (str);
}

static char	*comm_less(char *str)
{
  int		i;

  i = 0;
  while (str[i])
    {
      if (str[i] == COMMENT_CHAR)
	str[i] = '\0';
      i++;
    }
  return (space_less(str));
}

static char	*clean(char *instr, int l)
{
  int		k;
  char		*str;
  int		i;

  if ((str = malloc(sizeof(char) * my_strlen(instr) + 1)) == NULL)
    return (NULL);
  k = 0;
  i = l;
  while ((i < my_strlen(instr)) && instr[i])
    {
      if (instr[i] == ' ' || instr[i] == '\t' || instr[i] == COMMENT_CHAR)
	{
	  if (i != l)
	    str[k++] = ' ';
	  while (instr[i] && (instr[i] == ' ' || instr[i] == '\t'))
	    i++;
	}
      if (instr[i])
	str[k] = instr[i];
      k++;
      i++;
    }
  str[k] = '\0';
  return (comm_less(str));
}

char		*clean_instr(char *instr)
{
  int		i;

  i = 0;
  while (instr[i])
    {
      if (instr[i] == ':' &&
	  (instr[i + 1] == ' ' || instr[i + 1] == '\t' || instr[i + 1] == '\0'))
	return (clean(instr, i + 1));
      i++;
    }
  return (clean(instr, 0));
}
