/*
** is_label.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:43:44 2014 alexis abra
** Last update Sun Apr 13 18:05:34 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"
#include "my_printf.h"

static int	get_label_size(char *str, int i)
{
  int		k;

  k = 0;
  while (str[i] && str[i] != ',')
    {
      k++;
      i++;
    }
  return (k);
}

static char	*cp_label(t_list *list, int i)
{
  int		k;
  char		*str;

  if ((str = malloc(sizeof(char) * get_label_size(list->line, i) + 1)) == NULL)
    return (NULL);
  k = 0;
  while (list->line[i] && list->line[i] != ',')
    str[k++] = list->line[i++];
  str[k] = '\0';
  return (str);
}

int		is_label(t_list *root, t_list *list, int i)
{
  char		*tmp;
  t_list	*list_tmp;

  list_tmp = root->next;
  if ((tmp = cp_label(list, i)) == NULL)
    return (usage(NULL, MALLOCERR, 2));
  while (list_tmp != root)
    {
      if (my_strcmp(list_tmp->label, tmp) == 0)
	{
	  free(tmp);
	  return (SUCCESS);
	}
      list_tmp = list_tmp->next;
    }
  my_print_error("error line %d : \"%s\" , label \"%s\" doesn't exist!\n",
		 list->num_line, list->line, tmp);
  free(tmp);
  return (FAILURE);
}
