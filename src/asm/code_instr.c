/*
** code_instr.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:40:53 2014 alexis abra
** Last update Sun Apr 13 17:59:20 2014 alexis abra
*/

#include "my_printf.h"
#include "asm.h"
#include "tools.h"
#include "op.h"

static int	get_arg_value(t_list *list, t_list *root, t_coo *coo)
{
  if (coo->tab[coo->num_arg][0] == '%' && coo->tab[coo->num_arg][1] == ':')
    return (get_distance_to_label(coo, list, root));
  else if (coo->tab[coo->num_arg][0] == ':')
    return (get_distance_to_label(coo, list, root));
  else
    return (get_int_from_arg(coo->tab[coo->num_arg], list));
}

static int	fill_arg(t_coo *coo, char *code, t_list *list, t_list *root)
{
  int		nb_octet;

  nb_octet = octet_value(coo->tab, find_instru(coo->tab[0]), coo->num_arg);
  if (nb_octet == 4)
    int_to_char(coo, code, get_arg_value(list, root, coo));
  else if (nb_octet == 1)
    registr_to_char(coo, code, get_arg_value(list, root, coo));
  else
    shortint_to_char(coo, code, get_arg_value(list, root, coo));
  return (SUCCESS);
}

int		get_instru_code(char *code, t_list *list, t_list *root, t_coo *coo)
{
  int		num_instr;

  num_instr = find_instru(coo->tab[0]);
  coo->num_arg = 1;
  while (coo->num_arg < op_tab[num_instr].nbr_args + 1)
    {
      if (fill_arg(coo, code, list, root) == FAILURE)
        return (FAILURE);
      coo->pos = coo->pos + octet_value(coo->tab, num_instr, coo->num_arg);
      coo->num_arg++;
    }
  return (SUCCESS);
}
