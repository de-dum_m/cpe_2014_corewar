/*
** list_function.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:44:21 2014 alexis abra
** Last update Sun Apr 13 17:44:22 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"

void		rm_elem_list(t_list *elem)
{
  t_list        *tmp;

  tmp = elem;
  free(elem->line);
  free(elem->label);
  elem->prev->next = elem->next;
  tmp->next->prev = tmp->prev;
  free(elem);
}

int		free_list(t_list*root)
{
  t_list        *list;

  list = root->next;
  while (list != root)
    {
      rm_elem_list(list);
      list = list->next;
    }
  rm_elem_list(root);
  return (SUCCESS);
}

t_list          *init_root(void)
{
  t_list        *root;

  if ((root = malloc(sizeof(t_list))) == NULL)
    return (NULL);
  if ((root->label = malloc(sizeof(char))) == NULL)
    return (NULL);
  if ((root->line = malloc(sizeof(char))) == NULL)
    return (NULL);
  root->next = root;
  root->prev = root;
  return (root);
}

int		creat_elem_with_line(t_list *list, char *str, int i)
{
  t_list        *new_elem;

  if ((new_elem = malloc(sizeof(t_list))) == NULL)
    return (usage(NULL, MALLOCERR, 2));
  if ((new_elem->line = clean_instr(str)) == NULL)
    return (usage(NULL, "instr_clean failed", 2));
  if ((new_elem->label = add_label_to_list(str)) == NULL)
    return (usage(NULL, MALLOCERR, 2));
  if (new_elem->line[0] != '\0' && new_elem->line[0] != '.')
    {
      if ((new_elem->nb_octet = line_valide(new_elem->line, i)) == FAILURE)
	return (usage(NULL, "line_valid failed", 2));
    }
    else
      new_elem->nb_octet = 0;
  new_elem->pos_label = 0;
  new_elem->num_line = i;
  new_elem->next = list;
  new_elem->prev = list->prev;
  list->prev->next = new_elem;
  list->prev = new_elem;
  return (SUCCESS);
}
