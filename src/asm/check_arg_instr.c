/*
** check_arg_instr.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:32:43 2014 alexis abra
** Last update Sun Apr 13 17:57:49 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"
#include "my_printf.h"

int	check_registre(char *str)
{
  if (my_strlen(str) == 2)
    {
      if (str[1] >= '0' && str[1] <= '9')
        return (1);
    }
  if (my_strlen(str) == 3)
    {
      if (str[1] != '1')
        return (FAILURE);
      if  (str[2] >= '0' && str[2] <= '6')
        return (1);
    }
  return (FAILURE);
}

int	check_type(char **tab, int num_arg)
{
  if (tab[num_arg][0] == 'r')
    {
      if (check_registre(tab[num_arg]) == 1)
        return (1);
      else
        return (FAILURE);
    }
  if (tab[num_arg][0] == '%')
    return (2);
  if (my_str_isnum(tab[num_arg]) == 1 || tab[num_arg][0] == ':')
    return (4);
  return (FAILURE);
}

int	have_index(char *str)
{
  if (str == NULL)
    return (-2);
  if (my_strcmp(str, "ldi") == 0)
    return (FAILURE);
  if (my_strcmp(str, "zjmp") == 0)
    return (FAILURE);
  if (my_strcmp(str, "fork") == 0)
    return (FAILURE);
  if (my_strcmp(str, "sti") == 0)
    return (FAILURE);
  return (SUCCESS);
}

int	have_a_codage_octet(char *str)
{
  if (str == NULL)
    return (-2);
  if (my_strcmp(str, "live") == 0)
    return (FAILURE);
  if (my_strcmp(str, "zjmp") == 0)
    return (FAILURE);
  if (my_strcmp(str, "fork") == 0)
    return (FAILURE);
  if (my_strcmp(str, "lfork") == 0)
    return (FAILURE);
  return (SUCCESS);
}

int	check_arg(char *str)
{
  char	**tab;
  int	num_instr;
  int	nb_arg_line;
  int	nb_octet;

  if ((tab = str_to_wordtab(str, ' ', ',')) == NULL)
    return (usage(NULL, MALLOCERR, 2));
  num_instr = find_instru(tab[0]);
  nb_arg_line = find_nb_arg_line(tab);
  if (op_tab[num_instr].nbr_args != nb_arg_line)
    return (FAILURE);
  if ((nb_octet = check_val_arg(tab, num_instr, nb_arg_line + 1)) == FAILURE)
    return (FAILURE);
  return (nb_octet);
}
