/*
** octet_value.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:46:53 2014 alexis abra
** Last update Sun Apr 13 18:08:59 2014 alexis abra
*/

#include "asm.h"
#include "tools.h"
#include "op.h"
#include "my_printf.h"

static int	if_label(char **tab)
{
  if (have_index(tab[0]) == FAILURE)
    return (2);
  return (4);
}

static int	if_live(char **tab)
{
  if (my_strcmp(tab[0], "live") == 0)
    return (4);
  return (2);
}

int		octet_value(char **tab, int num_instr, int num_arg)
{
  if (tab[num_arg][0] == 'r')
    {
      if (check_registre(tab[num_arg]) == 1)
        return (1);
      else
        return (FAILURE);
    }
  if (tab[num_arg][0] == '%')
    {
      if (op_tab[num_instr].type[num_arg - 1] & 4)
        return (if_label(tab));
      if (op_tab[num_instr].type[num_arg - 1] & 2)
        return (if_live(tab));
    }
  if (my_str_isnum(tab[num_arg]) == 1 || tab[num_arg][0] == ':')
    return (2);
  return (FAILURE);
}
