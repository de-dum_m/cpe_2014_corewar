/*
** line_valide.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:44:05 2014 alexis abra
** Last update Sun Apr 13 18:06:29 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"
#include "my_printf.h"

int	find_instru(char *str)
{
  int	i;

  i = 0;
  while (i < 16)
    {
      if (my_strcmp(str, op_tab[i].mnemonique) == 0)
	return (i);
      i++;
    }
  return (FAILURE);
}

int	find_nb_arg_line(char **tab)
{
  int	i;

  i = 0;
  while (tab[i])
    i++;
  return (i - 1);
}

int	get_nb_octet(char **tab, int num_instr, int nb_arg_line)
{
  int	i;
  int	nb_octet;

  nb_octet = 1;
  i = 1;
  while (i < nb_arg_line)
    {
      nb_octet = nb_octet + octet_value(tab, num_instr, i);
      i++;
    }
  return (nb_octet);
}

int	check_val_arg(char **tab, int num_instr, int nb_arg_line)
{
  int	i;
  int	nb_octet;

  nb_octet = 1;
  i = 1;
  while (i < nb_arg_line)
    {
      if (check_type(tab, i) == FAILURE)
	{
          return (FAILURE);
        }
      if (!(op_tab[num_instr].type[i-1] & check_type(tab, i)))
	{
	  return (FAILURE);
	}
      i++;
    }
  nb_octet = get_nb_octet(tab, num_instr, nb_arg_line);
  if (have_a_codage_octet(op_tab[num_instr].mnemonique) == -2)
    return (FAILURE);
  if (have_a_codage_octet(op_tab[num_instr].mnemonique) == SUCCESS)
    nb_octet++;
  return (nb_octet);
}

int	line_valide(char *str, int nb_line)
{
  int	nb_octet;
  int	octet_codage;
  char	**tab;

  if ((tab = str_to_wordtab(str, ' ', ',')) == NULL)
    return (usage(NULL, MALLOCERR, 2));
  if ((nb_octet = check_arg(str)) == FAILURE)
    {
      my_print_error("Syntax error at line %d : %s\n", nb_line, str);
      return (FAILURE);
    }
  if ((octet_codage = do_octet_codage(tab)) == FAILURE)
    {
      my_print_error("Syntax error at line %d : %s\n", nb_line, str);
      return (FAILURE);
    }
  return (nb_octet);
}
