/*
** write_header.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:47:41 2014 alexis abra
** Last update Sun Apr 13 17:47:42 2014 alexis abra
*/

#include "asm.h"
#include "tools.h"
#include "op.h"

int		write_header(header_t *header)
{
  int		res;

  res = (((header->magic >> 24) & 0xFF)
	 | ((header->magic << 8) & 0xFF0000)
	 | ((header->magic >> 8) & 0xFF00)
	 | ((header->magic << 24) & 0xFF000000));
  header->magic = res;
  res = (((header->prog_size >> 24) & 0xFF)
	 | ((header->prog_size << 8) & 0xFF0000)
	 | ((header->prog_size >> 8) & 0xFF00)
	 | ((header->prog_size << 24) & 0xFF000000));
  header->prog_size = res;
  return (SUCCESS);
}
