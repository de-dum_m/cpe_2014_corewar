/*
** fill_bin.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:41:47 2014 alexis abra
** Last update Sun Apr 13 17:41:51 2014 alexis abra
*/

#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "my_printf.h"
#include "asm.h"
#include "tools.h"
#include "op.h"

static int	fill_code(char *code, t_list *list, t_list *root)
{
  t_coo		*coo;

  if ((coo = malloc(sizeof(t_coo))) == NULL)
    return (usage(NULL, MALLOCERR, 2));
  if ((coo->tab = str_to_wordtab(list->line, ' ', ',')) == NULL)
    return (FAILURE);
  code[0] = op_tab[find_instru(coo->tab[0])].code;
  if (have_a_codage_octet(coo->tab[0]) == FAILURE)
    coo->pos = 1;
  else
    {
      coo->pos = 2;
      code[1] = do_octet_codage(coo->tab);
    }
  if (get_instru_code(code, list, root, coo) == FAILURE)
    return (FAILURE);
  free(coo);
  return (SUCCESS);
}

static int	my_write(t_list *list, t_list *root, t_my_size *size)
{
  char		*code;

  size->taille = list->nb_octet;
  if ((code = malloc(sizeof(char) * list->nb_octet)) == NULL)
    return (FAILURE);
  if (fill_code(code, list, root)  == FAILURE)
    return (FAILURE);
  if (my_concat(size->result, code, size) == FAILURE)
    return (FAILURE);
  return (SUCCESS);
}

static int	write_in_bin(int bin_fd, t_list *root, int prog_size, int mode)
{
  t_list	*list;
  t_my_size	*size;

  if ((size = malloc(sizeof(t_my_size))) == NULL)
    return (FAILURE);
  if ((size->result = malloc(sizeof(char *) * prog_size)) == NULL)
    return (FAILURE);
  size->total = 0;
  list = root->next;
  while (list != root)
    {
      if (list->line[0] != '\0' && list->line[0] != '.')
	{
	  if (my_write(list, root, size) == FAILURE)
	    return (usage(NULL, MALLOCERR, 2));
	}
      list = list->next;
    }
  if (rev_code(size->result, prog_size, bin_fd, mode) == FAILURE)
    return (usage(NULL, "write failed!", 2));
  return (SUCCESS);
}

int		fill_bin(t_list *root, char *name, int mode)
{
  int		bin_fd;
  header_t	*header;

  if ((header = fill_header(root)) == NULL)
    return (usage(NULL, "fill_header failed", 2));
  if ((bin_fd = open(new_name(name)
		     , O_CREAT | O_WRONLY | O_TRUNC, PERM)) == FAILURE)
    return (usage(NULL, "open failed", 2));
  if (write_header(header) == FAILURE)
    return (usage(NULL, "write_header failed", 2));
  if (write(bin_fd, header, sizeof(*header)) == FAILURE)
    return (usage(NULL, "write failed", 2));
  header->prog_size = (((header->prog_size >> 24) & 0xFF)
		       | ((header->prog_size << 8) & 0xFF0000)
		       | ((header->prog_size >> 8) & 0xFF00)
		       | ((header->prog_size << 24) & 0xFF000000));
  if (write_in_bin(bin_fd, root, header->prog_size, mode) == FAILURE)
    return (usage(NULL, "write_in_bin failed", 2));
  my_printf("Compiled successfully %s from %s !\n\tcomment = %s\n",
	    header->prog_name, name, header->comment);
  return (SUCCESS);
}
