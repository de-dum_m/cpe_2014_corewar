/*
** main.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:45:20 2014 alexis abra
** Last update Sun Apr 13 19:02:48 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"
#include "my_printf.h"

int	my_usage(void)
{
  my_print_error("Usage : ./asm -[mode] [arg].s ...\n\
\tmode = 'l' for little endian\n");
  return (FAILURE);
}

int	main(int ac, char **av)
{
  int	index;
  int	mode;

  index = 1;
  mode = 0;
  if (ac == 1)
    return (my_usage());
  if (my_strcmp(av[1], "-l") == 0)
    {
      index = 2;
      if (ac == index)
	return (my_usage());
      mode = 1;
    }
  if (my_check_arg(av, index) == FAILURE)
    return (FAILURE);
  while (av[index])
    {
      if (my_asm(av[index++], mode) == FAILURE)
	return (FAILURE);
      my_concat(NULL, NULL, NULL);
    }
  return (SUCCESS);
}
