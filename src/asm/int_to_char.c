/*
** int_to_char.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:43:27 2014 alexis abra
** Last update Sun Apr 13 18:04:44 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"
#include "my_printf.h"

void		int_to_char(t_coo *coo, char *code, int nb)
{
  int		i;
  int		mask;
  int		result;

  mask = 0xFF000000;
  i = 0;
  while (i < 4)
    {
      result = (nb & mask);
      result = result >> (24 - (i * 8));
      mask = mask >> 8;
      code[coo->pos + i] = (char)result;
      i++;
    }
}

void		shortint_to_char(t_coo *coo, char *code, short int nb)
{
  int		i;
  int		mask;
  short int	result;

  mask = 0x0000FF00;
  i = 0;
  while (i < 2)
    {
      result = (nb & mask);
      result = result >> (8 - (i * 8));
      mask = mask >> 8;
      code[coo->pos + i] = (char)result;
      i++;
    }
}

void		registr_to_char(t_coo *coo, char *code, char nb)
{
  int		i;
  int		mask;
  char		result;

  mask = 0x000000FF;
  i = 0;
  while (i < 1)
    {
      result = (nb & mask);
      code[coo->pos + i] = result;
      i++;
    }
}

static int	flow(char c, int res, t_list *list)
{
  if (c != '-' && res < 0)
    {
      my_print_error("Warning Overflow : Direct too big line %d : \"%s\"\n",
                     list->num_line, list->line);
      return (FAILURE);
    }
  if (c == '-' && res > 0)
    {
      my_print_error("Warning Underflow : Direct too small line %d : \"%s\"\n",
                     list->num_line, list->line);
      return (FAILURE);
    }
  return (SUCCESS);
}

int		get_int_from_arg(char *str, t_list *list)
{
  char		*tmp;
  int		i;
  int		res;

  i = 1;
  if (str[0] != '%' && str[0] != 'r')
    return (my_getnbr(str));
  if ((tmp = malloc(sizeof(char *) * my_strlen(str))) == NULL)
    return (usage(NULL, MALLOCERR, 2));
  while (str[i])
    {
      tmp[i - 1] = str[i];
      i++;
    }
  tmp[i - 1] = '\0';
  res = my_getnbr(tmp);
  if (flow(tmp[0], res, list) == FAILURE)
    {
      free(tmp);
      return (FAILURE);
    }
  free(tmp);
  return (res);
}
