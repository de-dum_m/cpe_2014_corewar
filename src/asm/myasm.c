/*
** myasm.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:45:53 2014 alexis abra
** Last update Sun Apr 13 18:58:52 2014 alexis abra
*/

#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "asm.h"
#include "tools.h"

int		my_asm(char *name, int mode)
{
  int		src_fd;
  t_list	*root;

  if ((src_fd = open(name, O_RDONLY)) == FAILURE)
    return (usage(NULL, "open failed", 2));
  if ((root = init_root()) == NULL)
    return (usage(NULL, "init_root failed", 2));
  if (fill_list(root, src_fd) == FAILURE)
    return (usage(NULL, "fill_list failed", 2));
  if (fill_bin(root, name, mode) == FAILURE)
    return (usage(NULL, "fill_bin failed", 2));
  free_list(root);
  return (SUCCESS);
}
