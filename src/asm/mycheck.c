/*
** mycheck.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:46:17 2014 alexis abra
** Last update Sun Apr 13 18:57:30 2014 alexis abra
*/

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "my_printf.h"
#include "tools.h"
#include "asm.h"

int	my_check_arg(char **av, int num_arg)
{
  int	index;
  int	count;
  int	fd;
  char	temp[3];

  index = num_arg;
  temp[2] = 0;
  while (av[index])
    {
      count = my_strlen(av[index]) - 1;
      temp[1] = av[index][count--];
      temp[0] = av[index][count];
      if (my_strcmp(temp, ".s") != 0)
	return (my_usage());
      if ((fd = open(av[index], O_RDONLY)) == FAILURE)
	{
	  my_print_error("File %s not accessible!\n", av[index]);
	  return (FAILURE);
	}
      close(fd);
      index++;
    }
  return (SUCCESS);
}
