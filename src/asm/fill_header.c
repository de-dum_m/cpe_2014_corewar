/*
** fill_header.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:42:13 2014 alexis abra
** Last update Sun Apr 13 17:42:15 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"

static int	fill_name(char *str, header_t *header)
{
  int		i;
  int		k;

  k = 0;
  i = 0;
  while (str[i])
    {
      if (str[i] == '"')
        {
          i++;
          while (str[i] && (str[i] != '"'))
            {
              if (k < PROG_NAME_LENGTH - 1)
		header->prog_name[k] = str[i];
              else
                return
		  (usage(NULL, ".name non valid : cf PROG_NAME_LENGHT dans op.h ", 2));
              i++;
	      k++;
            }
          header->prog_name[k] = '\0';
        }
      i++;
    }
  return (SUCCESS);
}

int		get_name(header_t *header, t_list *root)
{
  t_list	*list;
  char		name[5];
  int		i;

  list = root->next;
  while (list != root)
    {
      if (my_strlen(list->line) > 5)
        {
          if (list->line[0] == '.')
            {
              i = 0;
              while (i != 4)
		{
                  name[i] = list->line[i + 1];
                  i++;
                }
              name[i] = '\0';
              if (my_strcmp("name", name) == 0)
                return (fill_name(list->line, header));
            }
        }
      list = list->next;
    }
  return (usage(NULL, "Program need a name = .name \"NAME\"", 2));
}

static int	fill_comment(char *str, header_t *header)
{
  int		i;
  int		k;

  k = 0;
  i = 0;
  while (str[i])
    {
      if (str[i] == '"')
        {
          i++;
          while (str[i] && (str[i] != '"'))
            {
	      if (k < COMMENT_LENGTH)
                header->comment[k] = str[i];
              else
		return
		  (usage(NULL, "Comment too long : cf op.h -> COMMENT_LENGTH", 2));
	      i++;
	      k++;
            }
          header->comment[k] = '\0';
        }
      i++;
    }
  return (SUCCESS);
}

int		get_comment(header_t *header, t_list *root)
{
  t_list	*list;
  char		comment[8];
  int		i;

  list = root->next;
  while (list != root)
    {
      if (my_strlen(list->line) > 8)
        {
          if (list->line[0] == '.')
            {
              i = 0;
              while (i != 7)
                {
                  comment[i] = list->line[i + 1];
                  i++;
                }
              comment[i] = '\0';
              if (my_strcmp(comment, "comment") == 0)
                return (fill_comment(list->line, header));
            }
        }
      list = list->next;
    }
  return (-2);
}

int		get_prog_size(t_list *root)
{
  int		size;
  t_list	*list;

  list = root->next;
  size = 0;
  while (list != root)
    {
      size = size + list->nb_octet;
      list = list->next;
    }
  return (size);
}
