/*
** parseur.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:47:08 2014 alexis abra
** Last update Sun Apr 13 18:09:46 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"
#include "get_next_line.h"
#include "my_printf.h"

static char	*get_label(char *str, char *label)
{
  int		k;
  int		i;
  int		j;

  i = 0;
  k = 0;
  j = 0;
  while (str[i])
    {
      while (str[i] == ' ' || str[i] == '\t')
	j = i++;
      if (str[i] == ':'
	  && (str[i + 1] == ' ' || str[i + 1] == '\t' || str[i + 1] == '\0'))
	{
	  while (k < (i - j))
	    {
	      label[k] = str[j + k];
	      k++;
	    }
	}
      i++;
    }
  label[k] = '\0';
  return (label);
}

char		*add_label_to_list(char *str)
{
  char		*label;

  if ((label = malloc(sizeof(char) * my_strlen(str) + 1)) == NULL)
    return (NULL);
  label = get_label(str, label);
  return (label);
}

static int	is_line_code(char *str)
{
  int		i;

  i = 0;
  while (str[i] == ' ' || str[i] == '\t')
    i++;
  if (str[i] == COMMENT_CHAR || str[i] == '\n' || str[i] == '\0')
    return (FAILURE);
  return (SUCCESS);
}

int		code_in_line(t_list *root, int src_fd)
{
  char		*str;
  int		i;

  i = 1;
  while ((str = get_next_line(src_fd)) != NULL)
    {
      if (is_line_code(str) == SUCCESS)
	{
	  if ((my_strcmp(clean_instr(str), ".extend") == 0) ||
	      (my_strcmp(clean_instr(str), ".code") == 0))
	    {
	      my_print_error("Interruption : %s not supported!\n"
			     , clean_instr(str));
	      return (FAILURE);
	    }
	  if (creat_elem_with_line(root, str, i) == FAILURE)
	    return (usage(NULL, "creat_elem_with line fail", 2));
	}
      else
	free(str);
      i++;
    }
  return (SUCCESS);
}
