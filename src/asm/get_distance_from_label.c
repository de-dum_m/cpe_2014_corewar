/*
** get_distance_from_label.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:42:46 2014 alexis abra
** Last update Sun Apr 13 18:02:33 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"
#include "my_printf.h"

static char	*get_instr_label(char *str)
{
  char		*label;
  int		i;
  int		k;

  k = 0;
  i = 0;
  if ((label = malloc(sizeof(char) * my_strlen(str))) == NULL)
    return (NULL);
  while (str[i] && str[i] != ':')
    i++;
  i++;
  while (str[i])
    label[k++] = str[i++];
  label[k] = '\0';
  return (label);
}

static int	up(t_list *root, t_list *list, char *label)
{
  int		distance;
  t_list	*tmp;

  tmp = list;
  distance = 0;
  while (tmp != root)
    {
      distance = distance + tmp->nb_octet;
      if (my_strcmp(label, tmp->label) == 0)
	return (-distance + list->nb_octet);
      tmp = tmp->prev;
    }
  return (FAILURE);
}

static int	down(t_list *root, t_list *list, char *label)
{
  int		distance;
  t_list	*tmp;

  tmp = list;
  distance = 0;
  while (tmp!= root)
    {
      if (my_strcmp(label, tmp->label) == 0)
	return (distance);
      distance = distance + tmp->nb_octet;
      tmp = tmp->next;
    }
  return (FAILURE);
}

int		get_distance_to_label(t_coo *coo, t_list *list, t_list *root)
{
  int		distance;
  char		*label;

  if ((label = get_instr_label(coo->tab[coo->num_arg])) == NULL)
    return (usage(NULL, MALLOCERR, 2));
  if ((distance = up(root, list, label)) == FAILURE)
    {
      if ((distance = down(root, list, label)) == FAILURE)
	return (FAILURE);
    }
  if (distance > IDX_MOD || distance < -IDX_MOD)
    {
      my_print_error("Warning Indirection to far line %d = %s\n",
		     list->num_line, list->line);
      return (FAILURE);
    }
  return (distance);
}
