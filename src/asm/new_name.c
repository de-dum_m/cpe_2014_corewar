/*
** new_name.c for new_name in /home/abraha_c/rendu/cpe_2014_corewar/src/tools
**
** Made by alexis abra
** Login   <abraha_c@epitech.net>
**
** Started on  Tue Mar 25 15:40:06 2014 alexis abra
** Last update Sun Apr 13 18:12:53 2014 
*/

#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "asm.h"
#include "tools.h"

char	*new_name(char *name)
{
  char	*str;
  int	size_name;

  size_name = my_strlen(name);
  if ((str = malloc(sizeof(char) * size_name + 5)) == NULL)
    {
      my_putstr(MALLOCERR, 2);
      return (NULL);
    }
  str[0] = '\0';
  name[size_name - 2] = '\0';
  my_strcat(str, name);
  return (my_strcat(str, ".cor"));
}
