/*
** clean_list.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:40:32 2014 alexis abra
** Last update Sun Apr 13 17:40:34 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"

int		clean_list(t_list *root)
{
  t_list	*list;
  char		*instr_clean;

  list = root->next;
  while (list != root)
    {
      if ((instr_clean = clean_instr(list->line)) == NULL)
	return (usage(NULL, "instr_clean failed", 2));
      list->line = instr_clean;
      list = list->next;
    }
  return (SUCCESS);
}
