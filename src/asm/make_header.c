/*
** make_header.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:45:35 2014 alexis abra
** Last update Sun Apr 13 17:45:37 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"

header_t	*fill_header(t_list *root)
{
  header_t	*header;
  int		comment;

  if ((header = malloc(sizeof(*header))) == NULL)
    return (NULL);
  header->magic = COREWAR_EXEC_MAGIC;
  if (get_name(header, root) == FAILURE)
    return (NULL);
  if ((comment = get_comment(header, root)) == FAILURE)
    return (NULL);
  if (comment == -2)
    header->comment[0] = '\0';
  if ((header->prog_size = get_prog_size(root)) == FAILURE)
    return (NULL);
  return (header);
}
