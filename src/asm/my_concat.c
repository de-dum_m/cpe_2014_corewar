/*
** my_concat.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:46:39 2014 alexis abra
** Last update Sun Apr 13 17:46:41 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"

int		my_concat(char *code, char *src, t_my_size *size)
{
  static int	i = 0;
  int		k;

  k = 0;
  if (code == NULL)
    {
      i = 0;
      return (SUCCESS);
    }
  while (k != size->taille)
    code[i++] = src[k++];
  return (SUCCESS);
}
