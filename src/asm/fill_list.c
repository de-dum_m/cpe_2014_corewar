/*
** fill_list.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:42:28 2014 alexis abra
** Last update Sun Apr 13 17:42:30 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"

static int	get_pos(t_list *root, int nb_label)
{
  t_list	*list;
  int		i;
  int		pos;

  list = root->next;
  pos = 0;
  i = 0;
  while (list != root && i != nb_label)
    {
      if (list->label[0] != '\0')
	i++;
      pos = pos + list->nb_octet;
      list = list->next;
    }
  return (pos);
}

static int	find_label_pos(t_list *root)
{
  t_list	*list;
  int		nb_label;

  list = root->next;
  nb_label = 1;
  while (list != root)
    {
      if (list->label[0] != '\0')
	list->pos_label = get_pos(root, nb_label++) - list->nb_octet;
      list = list->next;
    }
  return (SUCCESS);
}

static int	label_valid(t_list *root)
{
  t_list	*list;
  int		i;

  list = root->next;
  while (list != root)
    {
      i = 0;
      if (list->line[0] != '.')
	{
	  while (list->line[i])
	    {
	      if (list->line[i] == ':' ||
		  (list->line[i] == '%' && list->line[i + 1] == ':'))
		{
		  if (list->line[i] == '%' && list->line[i + 1] == ':')
		    i++;
		  if (is_label(root, list, i + 1) == FAILURE)
		    return (FAILURE);
		}
	      i++;
	    }
	}
      list = list->next;
    }
  return (SUCCESS);
}

int		fill_list(t_list *root, int src_fd)
{
  if (code_in_line(root, src_fd) == FAILURE)
    return (FAILURE);
  if (find_label_pos(root) == FAILURE)
    return (usage(NULL, "find_label_pos failed", 2));
  if (label_valid(root) == FAILURE)
    return (FAILURE);
  return (SUCCESS);
}
