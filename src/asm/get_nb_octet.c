/*
** get_nb_octet.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:43:10 2014 alexis abra
** Last update Sun Apr 13 17:43:12 2014 alexis abra
*/

#include <stdlib.h>
#include "asm.h"
#include "tools.h"
#include "op.h"

static int	is_function(char *str)
{
  int		i;

  i = 0;
  while (str[i])
    i++;
  if (str[i - 1] == LABEL_CHAR)
    return (SUCCESS);
  return (FAILURE);
}

static int	get_size(char *str)
{
  if (str[0] == COMMENT_CHAR)
    return (0);
  else if (str[0] == DIRECT_CHAR && str[1] && str[1] != LABEL_CHAR)
    return (T_DIR);
  else if (str[0] == 'r' && str[1] && str[1] <= '9' && str[1] >= '0')
    return (T_REG);
  else
    return (T_LAB);
}

static int	size_mnemonique(char *str)
{
  int		i;

  i = 0;
  while (my_strcmp(str, op_tab[i].mnemonique) != 0)
    i = i + 1;
  if (i == 8 || i == 11 || i == 14 || i == 15)
    return (2);
  return (1);
}

int		get_nb_oct(char *str)
{
  char		**tab;
  int		i;
  int		nb_octet;

  i = 0;
  if ((tab = str_to_wordtab(str, ' ', ',')) == NULL)
    return (FAILURE);
  if (is_function(tab[i]) == SUCCESS)
    i = 1;
  if (tab[i])
    nb_octet = size_mnemonique(tab[i]);
  i = i + 1;
  while (tab[i])
    {
      nb_octet = nb_octet + get_size(tab[i]);
      i = i + 1;
    }
  return (nb_octet);
}
