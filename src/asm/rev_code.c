/*
** rev_code.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:47:27 2014 alexis abra
** Last update Sun Apr 13 18:10:30 2014 alexis abra
*/

#include <unistd.h>
#include "asm.h"
#include "tools.h"
#include "op.h"

static int		write_little(char *str, int totale, int bin_fd)
{
  if (write(bin_fd, str, totale) == FAILURE)
    return (FAILURE);
  return (SUCCESS);
}

static int		write_big(char *str, int totale, int bin_fd)
{
  int			i;
  char			tmp;
  char			last[2];

  i = 0;
  while (i < totale - 1)
    {
      tmp = str[i];
      str[i] = str[i + 1];
      str[i + 1] = tmp;
      i = i + 2;
    }
  if (totale % 2 == 0)
    return (write(bin_fd, str, totale));
  else
    {
      last[0] = 0x00;
      last[1] = str[totale - 1];
      if (write(bin_fd, str, totale - 1) == FAILURE)
	return (FAILURE);
      if (write(bin_fd, &last, 2) == FAILURE)
	return (FAILURE);
    }
  return (SUCCESS);
}

int			rev_code(char *str, int totale, int bin_fd, int mode)
{
  if (mode == 1)
    return (write_little(str, totale, bin_fd));
  return (write_big(str, totale, bin_fd));
}
