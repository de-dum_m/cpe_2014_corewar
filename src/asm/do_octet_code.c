/*
** do_octet_code.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/asm
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:41:20 2014 alexis abra
** Last update Sun Apr 13 17:59:42 2014 alexis abra
*/

#include "asm.h"

char	do_octet_codage(char **tab)
{
  int	nb_arg;
  int	i;
  char	octet_codage;

  i = 1;
  nb_arg = find_nb_arg_line(tab);
  octet_codage = 0;
  while (i < nb_arg + 1)
    {
      if (check_type(tab, i) == 1)
        octet_codage = octet_codage | 1;
      else if (check_type(tab, i) == 2)
        octet_codage = octet_codage | 2;
      else
        octet_codage = octet_codage | 3;
      octet_codage = octet_codage << 2;
      i++;
    }
  while (i < 4)
    {
      octet_codage = octet_codage << 2;
      i++;
    }
  return (octet_codage);
}
