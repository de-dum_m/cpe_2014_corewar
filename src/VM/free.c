/*
** free.c for VM in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/src/VM
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Wed Apr  9 15:49:15 2014
** Last update Sun Apr 13 09:56:26 2014 
*/

#include <stdlib.h>
#include "vm.h"

void	my_freezer(t_vm *v, char *v_mem)
{
  int	index;

  index = 0;
  while (index < v->nbr_prog)
    {
      free(v->p[index]->r_name);
      free(v->p[index++]);
    }
  free(v->p);
  free(v_mem);
}
