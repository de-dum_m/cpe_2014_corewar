/*
** main.c for VM in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/VM
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue Mar 11 13:35:51 2014
** Last update Sun Apr 13 19:14:58 2014 
*/

#include <stdlib.h>
#include "vm.h"
#include "tools.h"
#include "get_next_line.h"

void		init_registers(t_prog *p)
{
  int		i;
  int		j;
  char		*str;

  i = 0;
  while (i < REG_NUMBER)
    {
      j = 0;
      while (j < REG_SIZE)
	p->reg[i][j++] = '\0';
      i++;
    }
  my_copy_int(str = my_int_to_str(p->p_num), p->reg[1]);
  free(str);
}

static int	init_t_prog(t_prog **progs)
{
  int		i;

  i = 0;
  while (i < 4)
    {
      if (!(progs[i] = malloc(sizeof(t_prog))))
	return (usage(NULL, MALLOCERR, STDERR));
      progs[i]->p_name = NULL;
      progs[i]->r_name = NULL;
      progs[i]->p_num = i + 1;
      progs[i]->t_alive_nb = i;
      progs[i]->load_addr = FAILURE;
      progs[i]->next_cycle = 1;
      progs[i]->last_live = 0;
      progs[i]->carry = 0;
      i++;
    }
  progs[i] = NULL;
  return (SUCCESS);
}

static int	init_struct(t_vm *v, t_prog **progs)
{
  t_op		*tion;

  v->p = progs;
  v->dump = FAILURE;
  v->little = FAILURE;
  if (!(tion = malloc(sizeof(t_op))))
    return (FAILURE);
  tion->debug = FAILURE;
  tion->debug = FAILURE;
  v->tion = tion;
  return (SUCCESS);
}

int		main(int ac, char **av)
{
  t_prog	**progs;
  t_vm		v;
  int		index;

  index = 0;
  if (!(progs = malloc(sizeof(t_prog) * 4)))
    return (usage(NULL, MALLOCERR, STDERR));
  if (init_t_prog(progs) == FAILURE)
    return (FAILURE);
  if (init_struct(&v, progs) == FAILURE)
     return (FAILURE);
  if ((put_param(ac, av, &v, 0)) == FAILURE)
     return (FAILURE);
  while (index < 4)
    init_registers(progs[index++]);
  if (v.nbr_prog < 2)
    return (usage(NULL, "needs at least a 2 progs...", STDERR));
  if (my_check(&v) == FAILURE)
    return (FAILURE);
  if (my_check(&v) == FAILURE)
    return (FAILURE);
     if (start_game(&v) == FAILURE)
    return (FAILURE);
  return (SUCCESS);
}
