/*
** replace_mem.c for VM in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Mar 29 18:39:16 2014 de-dum_m
** Last update Fri Apr 11 14:29:45 2014 
*/

#include "tools.h"
#include "vm.h"

static int	del_check(int actual_pos, int init_pos, char *v_mem, t_prog *p)
{
  static int	secu = -1;

  if (secu == p->p_num)
    return (FAILURE);
  secu = p->p_num;
  while (init_pos % MEM_SIZE != actual_pos)
    v_mem[init_pos++ % MEM_SIZE] = '\0';
  return (SUCCESS);
}

static int	replace_pos(int size, int max_size, int init_pos, int i)
{
  if (size > max_size)
    {
      max_size = size;
      init_pos = i;
    }
  return (init_pos + (max_size / 3));
}

int	replace_mem(int actual_pos, int init_pos, t_prog *p, char *v_mem)
{
  int	i;
  int	size;
  int	max_size;

  i = -1;
  max_size = 0;
  size = 0;
  if (del_check(actual_pos, init_pos, v_mem, p) == FAILURE)
    return (FAILURE);
  while (++i < MEM_SIZE)
    {
      if (v_mem[i] == '\0')
	size++;
      else
	{
	  if (size > max_size)
	    {
	      max_size = size;
	      init_pos = i;
	    }
	  size = 0;
	}
    }
  return (replace_pos(size, max_size, init_pos, i));
}
