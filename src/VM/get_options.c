/*
** get_options.c for VM in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Apr 12 15:07:08 2014 de-dum_m
** Last update Sun Apr 13 19:26:24 2014 
*/

#include <stdlib.h>
#include "vm.h"
#include "tools.h"

static int	print_usage()
{
  my_print_error("Usage : corewar [-dump nbr_cycle]\
[[-n prog_number] [-a load_address ] prog_name]\
 [options]...\n\
-l	little endian\n\
-s	step-by-step mode\n\
-d	debug mode\n\
-h	display this message\n");
  return (FAILURE);
}

static int	put_option_a(t_vm *v, char **av, int *index, int *prog_nb)
{
  if (av[*index + 1] && av[*index + 1][0] != '-' && *prog_nb < 4)
    {
      v->p[*prog_nb]->load_addr = my_getnbr(av[*index + 1]);
      *index = *index + 1;
      return (SUCCESS);
    }
  usage(NULL, "option not found", STDERR);
  return (FAILURE);
}

static int	put_option_n(t_vm *v, char **av, int *index, int *prog_nb)
{
  if (av[*index + 1] && av[*index + 1][0] != '-' && *prog_nb < 4)
    {
      v->p[*prog_nb]->p_num = my_getnbr(av[*index + 1]);
      *index = *index + 1;
      return (SUCCESS);
    }
  usage(NULL, "option not found", STDERR);
  return (FAILURE);
}

static char	**create_put_tab(int (*tab[8])(t_vm *, char **, int *, int *),
				 int index)
{
  char	*temp[8];
  char	**tab_name;

  tab[0] = &put_option_a;
  tab[1] = &put_option_n;
  tab[2] = &put_option_dump;
  tab[3] = &put_option_endian;
  tab[4] = &put_option_step;
  tab[5] = &put_option_debug;
  tab[6] = &print_usage;
  tab[7] = &put_prog_name;
  temp[0] = "-a";
  temp[1] = "-n";
  temp[2] = "-dump";
  temp[3] = "-l";
  temp[4] = "-s";
  temp[5] = "-d";
  temp[6] = "-h";
  if (!(tab_name = malloc(sizeof(char *) * 8)))
    return (NULL);
  while (++index < 7)
    if (!(tab_name[index] = my_str_copy(temp[index], 0)))
      return (NULL);
  tab[8] = NULL;
  return (tab_name);
 }

int	put_param(int ac, char **av, t_vm *v, int prog_nb)
{
  int	(*tab[8])(t_vm *, char **, int *, int *);
  int	index;
  int	count;
  char	**tab_name;

  index = 0;
  if (!(tab_name = create_put_tab(tab, index - 1)))
    return (FAILURE);
  while (++index < ac)
    {
      count = -1;
      while (++count < 8)
	if (count == 7)
	  {
	    if (tab[count](v, av, &index, &prog_nb) == FAILURE)
	      return (FAILURE);
	  }
	else if (my_strcmp(av[index], tab_name[count]) == 0)
	  {
	    if (tab[count](v, av, &index, &prog_nb) == FAILURE)
	      return (FAILURE);
	    count = 8;
	  }
    }
  return (v->nbr_prog = prog_nb);
}
