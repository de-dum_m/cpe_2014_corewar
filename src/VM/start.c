/*
** start.c for VM in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/src/VM
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Mar 17 09:29:04 2014
** Last update Sun Apr 13 19:19:00 2014 
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "vm.h"
#include "tools.h"
#include "op.h"

static int	load_inplace(t_prog *p, char *v_mem, char *tmp, int red)
{
  int		i;

  i = 0;
  tmp[red] = '\0';
  while (i < red)
    {
      if (v_mem[(p->load_addr + i) % MEM_SIZE] != '\0')
	{
	  if ((p->load_addr = replace_mem((p->load_addr + i) % MEM_SIZE,
					  p->load_addr % MEM_SIZE
					  , p, v_mem)) == FAILURE)
	    return (usage(p->p_name, " overlapping memory", STDERR));
	  i = 0;
	}
      v_mem[(p->load_addr + i) % MEM_SIZE] = tmp[i];
      i++;
    }
  return (SUCCESS);
}

static int	my_header_read(int fd, t_prog *p)
{
  int		red;
  header_t	stt;

  if ((red = read(fd, &stt, sizeof(header_t))) <= 0)
    return (usage(p->p_name, " open failed", STDERR));
  p->size = stt.prog_size;
  red = ((stt.magic>>24)&0xff)
    | ((stt.magic<<8)&0xff0000)
    | ((stt.magic>>8)&0xff00)
    | ((stt.magic<<24)&0xff000000);
  if (red != COREWAR_EXEC_MAGIC)\
    return (usage(p->p_name, " is not a corewar executable", STDERR));
  p->r_name = my_str_copy(stt.prog_name, 0);
  return (SUCCESS);
}

static int	load_cor_file(t_prog *p, char *v_mem, int nbr_prog, int nb)
{
  int		fd;
  int		i;
  int		red;
  char		tmp[MEM_SIZE];
  char		*pt_tmp;

  i = -1;
  if (p->p_name == NULL)
    return (SUCCESS);
  if (p->load_addr == -1)
    p->load_addr = nb * (MEM_SIZE / nbr_prog) % MEM_SIZE;
  if ((fd = open(p->p_name, O_RDONLY)) < 0)
    return (usage(p->p_name, " open failed", STDERR));
  if (my_header_read(fd, p) == FAILURE)
    return (FAILURE);
  if ((red = read(fd, tmp, MEM_SIZE)) <= 0
      || load_inplace(p, v_mem, tmp, red) == FAILURE)
    return (FAILURE);
  pt_tmp = my_int_to_str(p->load_addr);
  while (++i <= REG_SIZE)
    p->pc[i] = pt_tmp[i];
  free(pt_tmp);
  close(fd);
  return (SUCCESS);
}

int	start_game(t_vm *v)
{
  int	i;
  char	*v_mem;

  i = -1;
  if (!(v_mem = malloc(MEM_SIZE)) || !(v_mem = my_memset(v_mem, MEM_SIZE)))
    return (FAILURE);
  while (v->p[++i])
    if (v->p[i]->load_addr != -1
	&& load_cor_file(v->p[i], v_mem, v->nbr_prog, i) == FAILURE)
      return (FAILURE);
  i = -1;
  while (++i < v->nbr_prog)
    {
      if (v->p[i]->load_addr == -1
	  && load_cor_file(v->p[i], v_mem, v->nbr_prog, i) == FAILURE)
	return (FAILURE);
      else
	v->t_alive[i]++;
    }
  game_on(v, v_mem, 0);
  return (SUCCESS);
}
