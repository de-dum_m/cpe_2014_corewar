/*
** do_processes.c for VM in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/src/VM
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Thu Apr 10 11:39:33 2014
** Last update Sun Apr 13 18:07:53 2014 
*/

#include <stdlib.h>
#include "op.h"
#include "tools.h"
#include "vm.h"
#include "get_next_line.h"

static void	bad_param(t_vm *v, int i)
{
  char		*str;

  if (v->tion->debug == 1)
    my_printf("Bad parameter at %s\n", v->p[i]->pc);
  my_copy_int(str = my_int_to_str((my_getnbr_reg(v->p[i]->pc) + 1) % MEM_SIZE),
	      v->p[i]->pc);
  free(str);
}

int	count_lives(t_vm *v)
{
  if (v->lives_done >= NBR_LIVE)
    {
      v->cycles_left -= ((v->lives_done / NBR_LIVE) * CYCLE_DELTA);
      v->lives_done = v->lives_done % NBR_LIVE;
    }
  if (v->cycles_left > 0)
    return (SUCCESS);
  return (FAILURE);
}

void		do_processes(t_vm *v, char *v_mem, int cycles,
			     void (*tab[16])(t_vm *, char *, int, int))
{
  int		i;
  int		printed;
  int		op;

  i = -1;
  printed = 0;
  while (++i < v->nbr_prog && count_lives(v) == SUCCESS)
    if (v->p[i] && v->p[i]->next_cycle <= cycles && v->p[i]->last_live != -1)
      {
	if (v->tion->debug == SUCCESS && !printed++)
	  my_printf("\n#####[%d]#####\n", cycles);
	if (v->tion->step == SUCCESS)
	  free(get_next_line(0));
	op = v_mem[my_getnbr_reg(v->p[i]->pc) % MEM_SIZE] - 1;
	if (op < 16 && op >= 0)
	  {
	    v->p[i]->next_cycle = cycles + op_tab[op].nbr_cycles;
	    tab[op](v, v_mem, cycles, i);
	    if (v->tion->debug == 1)
	      my_printf("== %d(%s) == %s done @ %s\n", v->p[i]->p_num,
			v->p[i]->r_name, op_tab[op].mnemonique, v->p[i]->pc);
	  }
	else
	  bad_param(v, i);
      }
}
