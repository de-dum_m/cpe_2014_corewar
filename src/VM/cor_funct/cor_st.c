/*
** cor_st_ld.c for cor_funct in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/src/VM/cor_funct
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Apr  6 14:30:57 2014
** Last update Sun Apr 13 18:00:47 2014 de-dum_m
*/

#include <stdlib.h>
#include "tools.h"
#include "vm.h"
#include "my_printf.h"

void	cor_st(t_vm *v, char *v_mem, int cycles, int i)
{
  int	x;
  int	p_cod;
  int	pos;
  int	tmp_cod;
  char	*arg1;
  char	*str;
  char	*to_reg;

  (void)(cycles);
  x = -1;
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc) + 1)) % MEM_SIZE];
  tmp_cod = (unsigned int)p_cod >> 6;
  arg1 = my_int_to_str(get_param_int(v, i, tmp_cod, &pos));
  to_reg = get_param(v, i, tmp_cod = (unsigned int)p_cod >> 4, &pos);
  if (to_reg && arg1)
    {
      while (++x < REG_SIZE)
	to_reg[x] = arg1[x] % 48;
      v->p[i]->carry = SUCCESS;
    }
  else
    v->p[i]->carry = 0;
  my_copy_int(str = my_int_to_str(++pos % MEM_SIZE), v->p[i]->pc);
  free(arg1);
  free(str);
}

void	cor_sti(t_vm *v, char *v_mem, int cycles, int i)
{
  int	p_cod;
  int	tmp_cod;
  int	pos;
  char	*tmp1;
  int	tmp2;
  int	tmp3;

  cycles = 0;
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc) + 1)) % MEM_SIZE];
  tmp_cod = (unsigned int)p_cod >> 6;
  if ((tmp1 = my_int_to_str(get_param_int(v, i, tmp_cod , &pos))))
    {
      tmp2 = get_param_index(v, i, tmp_cod = (unsigned int)p_cod >> 4, &pos);
      tmp3 = get_param_index(v, i, tmp_cod = (unsigned int)p_cod >> 2, &pos)
	+ tmp2;
      while (tmp1 && cycles < REG_SIZE && tmp3 > -1)
	{
	  v_mem[tmp3 %= MEM_SIZE] = tmp1[cycles++] - 48;
	  tmp3++;
	}
      v->p[i]->carry = SUCCESS;
    }
  else
    v->p[i]->carry = 0;
  my_copy_int(my_int_to_str(++pos % MEM_SIZE), v->p[i]->pc);
}
