/*
** funct_tools.c for cor_funct in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Mar 31 12:20:57 2014 de-dum_m
** Last update Sun Apr 13 18:17:20 2014 de-dum_m
*/

#include <stdlib.h>
#include "vm.h"
#include "tools.h"

int	add_prog(t_vm *v, int cycles, int i)
{
  if (!(v->p = realloc(v->p, (v->nbr_prog + 1) * sizeof(t_prog))))
    return (FAILURE);
  if (!(v->p[v->nbr_prog] = malloc(sizeof(t_prog))))
    return (usage(NULL, MALLOCERR, STDERR));
  v->p[v->nbr_prog]->p_name = v->p[i]->p_name;
  v->p[v->nbr_prog]->r_name = my_str_copy(v->p[i]->r_name, 0);
  v->p[v->nbr_prog]->t_alive_nb = v->p[i]->t_alive_nb;
  v->p[v->nbr_prog]->load_addr = FAILURE;
  v->p[v->nbr_prog]->next_cycle = cycles + op_tab[11].nbr_cycles;
  v->p[v->nbr_prog]->last_live = v->p[i]->last_live;
  v->p[v->nbr_prog]->carry = v->p[i]->carry;
  v->p[v->nbr_prog]->p_num = v->p[i]->p_num;
  v->p[v->nbr_prog]->father = FAILURE;
  init_registers(v->p[v->nbr_prog]);
  return (SUCCESS);
}

char	*get_param(t_vm *v, int i, int p_cod, int *pos)
{
  char	*tmp;

  if ((p_cod & 3) == 3)
    {
      tmp = v->v_mem + ABS(cor_getnbr(v->v_mem, IND_SIZE, pos) % IDX_MOD
				     + my_getnbr_reg(v->p[i]->pc));
    }
  else if ((p_cod & 3) == 2)
    {
      tmp = v->v_mem + ABS(cor_getnbr(v->v_mem, DIR_SIZE, pos) % IDX_MOD);
    }
  else if ((p_cod & 3) == 1)
    {
      *pos = (*pos + 1) % MEM_SIZE;
      if ((p_cod = v->v_mem[*pos]) < 16 && p_cod >= 0)
	tmp = v->p[i]->reg[p_cod];
      else
	return (NULL);
    }
  else
    return (NULL);
  return (tmp);
}

int	get_param_int(t_vm *v, int i, int p_cod, int *pos)
{
  int	tmp;

  tmp = -1;
  if ((p_cod & 3) == 3)
    {
      tmp = (cor_getnbr(v->v_mem, IND_SIZE, pos) % IDX_MOD
	     + my_getnbr_reg(v->p[i]->pc)) % MEM_SIZE;
    }
  else if ((p_cod & 3) == 2)
    {
      tmp = cor_getnbr(v->v_mem, DIR_SIZE, pos) % IDX_MOD;
    }
  else if ((p_cod & 3) == 1)
    {
      *pos = (*pos + T_REG) % MEM_SIZE;
      if ((p_cod = v->v_mem[*pos]) < 16)
	tmp = my_getnbr_reg(v->p[i]->reg[p_cod]);
    }
  return (tmp);
}

int	get_param_int_woidx(t_vm *v, int i, int p_cod, int *pos)
{
  int	tmp;

  tmp = -1;
  if ((p_cod & 3) == 3)
    {
      tmp = (cor_getnbr(v->v_mem, IND_SIZE, pos)
	     + my_getnbr_reg(v->p[i]->pc)) % MEM_SIZE;
    }
  else if ((p_cod & 3) == 2)
    {
      tmp = cor_getnbr(v->v_mem, DIR_SIZE, pos);
    }
  else if ((p_cod & 3) == 1)
    {
      *pos = (*pos + T_REG) % MEM_SIZE;
      if ((p_cod = v->v_mem[*pos]) < 16)
	tmp = my_getnbr_reg(v->p[i]->reg[p_cod]);
    }
  return (tmp);
}

int	get_param_index(t_vm *v, int i, int p_cod, int *pos)
{
  int	tmp;

  if ((p_cod & 3) == 3 || (p_cod & 3) == 2)
    {
      tmp = cor_getnbr(v->v_mem, IND_SIZE, pos) % MEM_SIZE;
    }
  else if ((p_cod & 3) == 1)
    {
      *pos = (*pos + T_REG) % MEM_SIZE;
      if ((p_cod = v->v_mem[*pos]) < 16)
	tmp = my_getnbr_reg(v->p[i]->reg[p_cod]);
      else
	tmp = -1;
    }
  return (tmp);
}
