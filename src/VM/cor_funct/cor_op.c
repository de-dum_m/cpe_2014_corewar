/*
** cor_op.c for cor_funct in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/src/VM/cor_funct
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Apr  6 14:28:06 2014
** Last update Sun Apr 13 18:50:18 2014 de-dum_m
*/

#include "tools.h"
#include "vm.h"

void	cor_add(t_vm *v, char *v_mem, int cycles, int i)
{
  int	nb;
  char	*reg3;
  char	*tmp;
  int	p_cod;
  int	tmp_cod;
  int	pos;
  int	x;

  x = -1;
  (void)cycles;
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc) + 1))];
  tmp_cod = (unsigned int)p_cod >> 6;
  nb = get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 4;
  nb += get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 2;
  reg3 = get_param(v, i, tmp_cod, &pos);
  tmp = my_int_to_str(nb);
  while (++x <= REG_SIZE && reg3 && nb)
    reg3[x] = tmp[x];
  pos++;
  my_copy_int(my_int_to_str(pos), v->p[i]->pc);
  v->p[i]->carry = 0;
  if (nb > 0)
    v->p[i]->carry = SUCCESS;
}

void	cor_sub(t_vm *v, char *v_mem, int cycles, int i)
{
  int	nb;
  char	*reg3;
  char	*tmp;
  int	p_cod;
  int	tmp_cod;
  int	pos;
  int	x;

  x = -1;
  (void)cycles;
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc) + 1))];
  tmp_cod = (unsigned int)p_cod >> 6;
  nb = get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 4;
  nb -= get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 2;
  reg3 = get_param(v, i, tmp_cod, &pos);
  tmp = my_int_to_str(nb);
  while (++x <= REG_SIZE && reg3 && nb)
    reg3[x] = tmp[x];
  pos++;
  my_copy_int(my_int_to_str(pos), v->p[i]->pc);
  v->p[i]->carry = 0;
  if (nb > 0)
    v->p[i]->carry = SUCCESS;
}

void	cor_and(t_vm *v, char *v_mem, int cycles, int i)
{
  int	nb;
  char	*reg3;
  char	*tmp;
  int	p_cod;
  int	tmp_cod;
  int	pos;
  int	x;

  x = -1;
  (void)cycles;
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc) + 1))];
  tmp_cod = (unsigned int)p_cod >> 6;
  nb = get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 4;
  nb &= get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 2;
  reg3 = get_param(v, i, tmp_cod, &pos);
  tmp = my_int_to_str(nb);
  while (++x <= REG_SIZE && reg3 && nb)
    reg3[x] = tmp[x];
  pos++;
  my_copy_int(my_int_to_str(pos), v->p[i]->pc);
  v->p[i]->carry = 0;
  if (nb > 0)
    v->p[i]->carry = SUCCESS;
}

void	cor_xor(t_vm *v, char *v_mem, int cycles, int i)
{
  int	nb;
  char	*reg3;
  char	*tmp;
  int	p_cod;
  int	tmp_cod;
  int	pos;
  int	x;

  x = -1;
  (void)cycles;
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc) + 1))];
  tmp_cod = (unsigned int)p_cod >> 6;
  nb = get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 4;
  nb ^= get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 2;
  reg3 = get_param(v, i, tmp_cod, &pos);
  tmp = my_int_to_str(nb);
  while (++x <= REG_SIZE && reg3 && nb)
    reg3[x] = tmp[x];
  pos++;
  my_copy_int(my_int_to_str(pos), v->p[i]->pc);
  v->p[i]->carry = 0;
  if (nb > 0)
    v->p[i]->carry = SUCCESS;
}

void	cor_or(t_vm *v, char *v_mem, int cycles, int i)
{
  int	nb;
  char	*reg3;
  char	*tmp;
  int	p_cod;
  int	tmp_cod;
  int	pos;
  int	x;

  x = -1;
  (void)cycles;
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc) + 1))];
  tmp_cod = (unsigned int)p_cod >> 6;
  nb = get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 4;
  nb |= get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 2;
  reg3 = get_param(v, i, tmp_cod, &pos);
  tmp = my_int_to_str(nb);
  while (++x <= REG_SIZE && reg3 && nb)
    reg3[x] = tmp[x];
  pos++;
  my_copy_int(my_int_to_str(pos), v->p[i]->pc);
  v->p[i]->carry = 0;
  if (nb > 0)
    v->p[i]->carry = SUCCESS;
}
