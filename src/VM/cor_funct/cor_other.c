/*
** cor_other.c for cor_funct in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/src/VM/cor_funct
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Apr  6 14:30:38 2014
** Last update Sun Apr 13 19:27:21 2014 
*/

#include <stdlib.h>
#include "vm.h"
#include "tools.h"

void	cor_zjump(t_vm *v, char *v_mem, int cycles, int i)
{
  int	jump;
  char	*str;

  (void)cycles;
  (void)v_mem;
  jump = my_getnbr_reg(v->p[i]->pc) + v_mem[(my_getnbr_reg(v->p[i]->pc)
					+ IND_SIZE) % MEM_SIZE] % IDX_MOD;
  if (v->p[i]->carry == SUCCESS)
    {
      my_copy_int(str = my_int_to_str(jump), v->p[i]->pc);
    }
  else
    my_copy_int(str = my_int_to_str((my_getnbr_reg(v->p[i]->pc)
				     + IND_SIZE + 2) % MEM_SIZE), v->p[i]->pc);
  free(str);
}

void	cor_fork(t_vm *v, char *v_mem, int cycles, int i)
{
  (void)cycles;
  add_prog(v, cycles, i);
  my_copy_int(my_int_to_str(v_mem[(my_getnbr_reg(v->p[i]->pc) + 2) %
				  MEM_SIZE] % IDX_MOD), v->p[v->nbr_prog]->pc);
  my_copy_int(my_int_to_str((my_getnbr_reg(v->p[i]->pc) + 3) %
			    MEM_SIZE), v->p[i]->pc);
  v->nbr_prog++;
  v->t_alive[v->p[i]->t_alive_nb]++;
}

void	cor_lfork(t_vm *v, char *v_mem, int cycles, int i)
{
  (void)cycles;
  add_prog(v, cycles, i);
  my_copy_int(my_int_to_str(v_mem[(my_getnbr_reg(v->p[i]->pc) + 2) % MEM_SIZE]),
	      v->p[v->nbr_prog]->pc);
  my_copy_int(my_int_to_str((my_getnbr_reg(v->p[i]->pc) + 3) % MEM_SIZE),
	      v->p[i]->pc);
  v->nbr_prog++;
  v->t_alive[v->p[i]->t_alive_nb]++;
}

void	cor_live(t_vm *v, char *v_mem, int cycles, int i)
{
  int	x;
  int	pos;
  int	lived;
  int	live_nb;
  char	*str;

  x = -1;
  lived = 0;
  pos = my_getnbr(v->p[i]->pc);
  live_nb = cor_getnbr(v_mem, 4, &pos);
  v->lives_done++;
  if (v->p[i]->p_num == live_nb && ++lived)
    my_printf("Le joueur %d(%s) est en vie (%d).\n", v->p[i]->p_num,
	      v->p[i]->r_name, v->p[i]->last_live = cycles);
  while (!lived && ++x < v->nbr_prog && live_nb > -1)
    if (v->p[x] && v->p[x]->last_live != FAILURE
	&& v->p[x]->p_num == live_nb && !lived++)
      my_printf("Le joueur %d(%s) est en vie (%d).\n", v->p[x]->p_num,
		v->p[x]->r_name, v->p[x]->last_live = cycles);
  my_copy_int(str = my_int_to_str(++pos % MEM_SIZE), v->p[i]->pc);
  free(str);
}

void	cor_aff(t_vm *v, char *v_mem, int cycles, int i)
{
  int	reg1;
  int	p_cod;
  int	tmp_cod;
  int	pos;
  char	*str;

  (void)cycles;
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc) + 1)) % MEM_SIZE];
  tmp_cod = (unsigned int)p_cod >> 6;
  reg1 = get_param_int(v, i, tmp_cod, &pos) % 256;
  if (v->tion->debug != SUCCESS)
    my_printf("%c\n", reg1);
  else
    my_printf("AFF = %c\n", reg1);
  my_copy_int(str = my_int_to_str(++pos % MEM_SIZE), v->p[i]->pc);
  free(str);
}
