/*
** cor_getnbr.c for cor_funct in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Apr 13 11:22:15 2014 de-dum_m
** Last update Sun Apr 13 14:12:37 2014 de-dum_m
*/

#include "vm.h"

int	cor_getnbr(char *v_mem, int size, int *pos)
{
  int	red;
  int	res;

  res = 0;
  red = 0;
  while (red < size)
    {
      *pos = (*pos + 1) % MEM_SIZE;
      res += BABS(v_mem[*pos]);
      if (++red < size)
	res <<= 4;
    }
  return (res);
}
