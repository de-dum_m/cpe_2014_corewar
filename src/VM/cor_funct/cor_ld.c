/*
** cor_st_ld.c for cor_funct in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/src/VM/cor_funct
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Apr  6 14:30:57 2014
** Last update Sun Apr 13 18:41:53 2014 de-dum_m
*/

#include <stdlib.h>
#include "tools.h"
#include "vm.h"
#include "my_printf.h"

void	cor_ld(t_vm *v, char *v_mem, int cycles, int i)
{
  int	x;
  int	to_reg;
  int	p_cod;
  int	tmp_cod;
  int	pos;
  int	tmp;
  char	*tofr;

  (void)(cycles);
  x = 0;
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc)) + 1) % MEM_SIZE];
  tmp_cod = (unsigned int)p_cod >> 6;
  tmp = get_param_int(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 4;
  to_reg = get_param_int(v, i, tmp_cod, &pos);
  v->p[i]->carry = 0;
  if (to_reg && to_reg < 16 && to_reg > -1)
    {
      while (x < REG_SIZE)
	v->p[i]->reg[to_reg][x++] = v->v_mem[tmp++];
      if (x == REG_SIZE && v->v_mem[tmp] > 0)
	v->p[i]->carry = SUCCESS;
    }
  my_copy_int(tofr = my_int_to_str(++pos % MEM_SIZE), v->p[i]->pc);
  free(tofr);
}

void	cor_lld(t_vm *v, char *v_mem, int cycles, int i)
{
  int	x;
  int	to_reg;
  int	p_cod;
  int	tmp_cod;
  int	pos;
  int	tmp;

  (void)(cycles);
  x = 0;
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc)) + 1) % MEM_SIZE];
  tmp_cod = (unsigned int)p_cod >> 6;
  tmp = get_param_int_woidx(v, i, tmp_cod, &pos);
  tmp_cod = (unsigned int)p_cod >> 4;
  to_reg = get_param_int_woidx(v, i, tmp_cod, &pos);
  v->p[i]->carry = 0;
  if (to_reg < 16 && to_reg && tmp > -1)
    {
      tmp %= MEM_SIZE;
      while (x < REG_SIZE)
	v->p[i]->reg[to_reg][x++] = v->v_mem[tmp++];
      v->p[i]->carry = SUCCESS;
      if (x == REG_SIZE && v->v_mem[tmp] > 0)
	v->p[i]->carry = SUCCESS;
    }
  my_copy_int(my_int_to_str(++pos % MEM_SIZE), v->p[i]->pc);
}

void	cor_ldi(t_vm *v, char *v_mem, int cycles, int i)
{
  int	x;
  int	p_cod;
  int	pos;
  int	res;
  int	tmp1;
  int	tmp2;
  char	*tmp3;

  (void)(cycles);
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc) + 1)) % MEM_SIZE];
  tmp1 = get_param_index(v, i, x = (unsigned int)p_cod >> 6, &pos);
  tmp2 = get_param_index(v, i, x = (unsigned int)p_cod >> 4, &pos);
  tmp3 = get_param(v, i, x = (unsigned int)p_cod >> 2, &pos);
  res = (tmp1 + tmp2) % IDX_MOD;
  while (x < REG_SIZE && res > -1)
    {
      res %= MEM_SIZE;
      tmp3[x] = v_mem[res] + 48;
      x++;
    }
  my_copy_int(my_int_to_str(++pos % MEM_SIZE), v->p[i]->pc);
}

void	cor_lldi(t_vm *v, char *v_mem, int cycles, int i)
{
  int	x;
  int	p_cod;
  int	pos;
  int	res;
  int	tmp1;
  int	tmp2;
  char	*tmp3;

  (void)(cycles);
  p_cod = v_mem[(pos = (my_getnbr_reg(v->p[i]->pc) + 1)) % MEM_SIZE];
  tmp1 = get_param_index(v, i, x = (unsigned int)p_cod >> 6, &pos);
  tmp2 = get_param_index(v, i, x = (unsigned int)p_cod >> 4, &pos);
  tmp3 = get_param(v, i, x = (unsigned int)p_cod >> 2, &pos);
  res = (tmp1 + tmp2) % MEM_SIZE;
  while (x < REG_SIZE && tmp3 && res > -1)
    {
      res %= MEM_SIZE;
      tmp3[x] = v_mem[res] + 48;
      x++;
    }
  my_copy_int(my_int_to_str(++pos % MEM_SIZE), v->p[i]->pc);
}
