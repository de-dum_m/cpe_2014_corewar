/*
** check_end.c for VM in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Fri Apr 11 22:29:00 2014
** Last update Fri Apr 11 22:30:22 2014 
*/

#include "tools.h"
#include "vm.h"

int	continue_or_not(t_vm *v)
{
  int	i;
  int	progs;

  i = 0;
  progs = 0;
  while (v->t_alive && i < REG_SIZE)
    {
      if (v->t_alive[i] > 0)
        progs++;
      i++;
    }
  if (progs > 1)
    return (SUCCESS);
  return (FAILURE);
}

int	init_t_alive(t_vm *v)
{
  int	i;

  i = 0;
  while (i < REG_SIZE)
    {
      if (i < v->nbr_prog)
        v->t_alive[i] = 1;
      else
        v->t_alive[i] = 0;
      i++;
    }
  return (SUCCESS);
}
