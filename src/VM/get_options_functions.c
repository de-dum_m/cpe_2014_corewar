/*
** get_options.c for VM in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Apr 12 15:07:08 2014 de-dum_m
** Last update Sun Apr 13 19:25:32 2014 
*/

#include <stdlib.h>
#include "vm.h"
#include "tools.h"

int	put_option_dump(t_vm *v, char **av, int *index, int *prog_nb)
{
  (void)(prog_nb);
  if (av[*index + 1] && av[*index + 1][0] != '-')
    {
      v->dump = my_getnbr(av[*index + 1]);
      *index = *index + 1;
      return (SUCCESS);
    }
  usage(NULL, "option not found", STDERR);
  return (FAILURE);
}

int	put_option_endian(t_vm *v, char **av, int *index, int *prog_nb)
{
  (void)(av);
  (void)(prog_nb);
  (void)(index);
  v->little = SUCCESS;
  return (SUCCESS);
}

int	put_option_step(t_vm *v, char **av, int *index, int *prog_nb)
{
  (void)(av);
  (void)(prog_nb);
  (void)(index);
  v->tion->step = SUCCESS;
  return (SUCCESS);
}

int	put_option_debug(t_vm *v, char **av, int *index, int *prog_nb)
{
  (void)(av);
  (void)(prog_nb);
  (void)(index);
  v->tion->debug = SUCCESS;
  return (SUCCESS);
}

int	put_prog_name(t_vm *v, char **av, int *index, int *prog_nb)
{
  if (*prog_nb == 4)
    return (usage(NULL, "Too mamy progs", STDERR));
  v->p[*prog_nb]->p_name = av[*index];
  *prog_nb = *prog_nb + 1;
  return (SUCCESS);
}
