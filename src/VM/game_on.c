/*
** game_on.c for VM in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Mar 28 17:59:23 2014 de-dum_m
** Last update Sun Apr 13 18:06:47 2014 
*/

#include <stdlib.h>
#include "op.h"
#include "tools.h"
#include "vm.h"
#include "get_next_line.h"

static void	check_lives(t_vm *v, int cycles, int cycles_left)
{
  int		i;

  i = 0;
  if (v->tion->debug == SUCCESS)
    my_printf("#####[%d]#####\n", cycles);
  if (v->tion->debug == 1)
    my_putstr("Check players still alive\n", 1);
  while (i < v->nbr_prog)
    {
      if (cycles - v->p[i]->last_live > cycles_left || v->p[i]->last_live == 0)
	{
	  if (v->tion->debug == 1 && v->p[i]->father == SUCCESS)
	    my_printf("\tProgramme %d(%s) is dead\n", v->p[i]->p_num,
		      v->p[i]->r_name);
	  v->t_alive[v->p[i]->t_alive_nb]--;
	  my_swap(v->p[i], v->p[v->nbr_prog - 1]);
	  free(v->p[v->nbr_prog - 1]);
	  v->p[v->nbr_prog - 1] = NULL;
	  v->nbr_prog--;
	}
      else
	i++;
    }
}

static void	p_tab(void (*tab[16])(t_vm *, char *, int, int))
{
  tab[0] = &cor_live;
  tab[1] = &cor_ld;
  tab[2] = &cor_st;
  tab[3] = &cor_add;
  tab[4] = &cor_sub;
  tab[5] = &cor_and;
  tab[6] = &cor_or;
  tab[7] = &cor_xor;
  tab[8] = &cor_zjump;
  tab[9] = &cor_ldi;
  tab[10] = &cor_sti;
  tab[11] = &cor_fork;
  tab[12] = &cor_lld;
  tab[13] = &cor_lldi;
  tab[14] = &cor_lfork;
  tab[15] = &cor_aff;
}

static void	end_game(int cycles, t_vm *v, char *v_mem)
{
  int		index;
  int		last_live;
  int		count;

  index = 0;
  last_live = -1;
  while (index < v->nbr_prog)
    {
      if (v->p[index]->last_live > last_live)
	{
	  last_live = v->p[index]->last_live;
	  count = index;
	}
      index++;
    }
  if (cycles == v->dump)
    my_print_mem(v_mem);
  if (last_live > 0)
    my_printf("[%d] Le joueur %d(%s) a gagné (%d)\n",
	      cycles, v->p[count]->p_num, v->p[count]->r_name,
	      last_live);
  else
    my_putstr("Draw\n", 1);
  my_freezer(v, v_mem);
}

static void	reverse_mem(t_vm *v, char *v_mem)
{
  int		i;
  char		tmp;

  i = 0;
  while (v->little == FAILURE && i < MEM_SIZE)
    {
      tmp = v_mem[i];
      v_mem[i] = v_mem[i + 1];
      i++;
      v_mem[i++] = tmp;
    }
}

int	game_on(t_vm *v, char *v_mem, int cycles)
{
  void	(*tab[16])(t_vm *, char *, int, int);

  p_tab(tab);
  v->cycles_left = CYCLE_TO_DIE + 1;
  v->lives_done = 0;
  reverse_mem(v, v_mem);
  v->v_mem = v_mem;
  init_t_alive(v);
  while (continue_or_not(v) != FAILURE && count_lives(v) == SUCCESS
	 && ((v->dump > 0 && cycles < v->dump) || v->dump == -1))
    {
      cycles++;
      if (cycles % v->cycles_left == 0)
       	check_lives(v, cycles, v->cycles_left);
      do_processes(v, v_mem, cycles, tab);
    }
  end_game(cycles, v, v_mem);
  return (SUCCESS);
}
