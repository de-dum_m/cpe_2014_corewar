/*
** mycheck.c for VM in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue Mar 11 13:37:11 2014 de-dum_m
** Last update Sun Apr 13 18:05:40 2014 
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "vm.h"
#include "tools.h"

static int	check_file(t_vm *vm, char temp[4], int index)
{
  int		fd;
  char		str[4];
  int		nb;

  temp[4] = 0;
  if (my_strcmp(temp, ".cor") != 0)
    return (usage(vm->p[index]->p_name, " is not a corewar executable", 2));
  if ((fd = open(vm->p[index]->p_name, O_RDONLY)) == FAILURE)
    return (usage(vm->p[index]->p_name, " is not accessible", 2));
  if ((nb = read(fd, str, 4)) == -1)
    return (FAILURE);
  close(fd);
  if (str[0] != 0 || str[1] != -22 || str[2] != -125 || str[3] != -13)
    return (usage(vm->p[index]->p_name, " is not a corewar executable", 2));
  return (SUCCESS);
}

static int	my_check_cor(t_vm *vm)
{
  int		index;
  int		nb;
  int		count;
  char		temp[4];

  index = 0;
  while (vm->p[index] && vm->p[index]->p_name)
    {
      nb = my_strlen(vm->p[index]->p_name) - 1;
      count = 3;
      while (count >= 0)
	{
	  temp[count] = vm->p[index]->p_name[nb--];
	  count--;
	}
      if (check_file(vm, temp, index) == FAILURE)
	return (FAILURE);
      index++;
    }
  return (SUCCESS);
}

int	my_check(t_vm *vm)
{
  int	i;
  int	j;

  i = 0;
  while (i < 4)
    {
      j = 0;
      if (vm->p[i] && !(vm->p[i]->p_name) && (vm->p[i]->p_num != i + 1
      				  || vm->p[i]->load_addr != FAILURE))
      	return (usage(NULL, "bad arg", 2));
      while (j < 4)
	{
	  if (i != j && vm->p[i] && vm->p[j]
	      && vm->p[i]->p_num == vm->p[j]->p_num)
	    return (usage(NULL, "Cannot assign same prog numebr twice", 2));
	  j++;
	}
      i++;
    }
  vm->p[vm->nbr_prog] = NULL;
  return (my_check_cor(vm));
}
