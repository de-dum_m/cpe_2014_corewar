/*
** sort.c for VM in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/src/VM
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Mar 30 11:36:55 2014
** Last update Sun Apr 13 18:09:12 2014 
*/

#include "vm.h"

void		my_swap(t_prog *p1, t_prog *p2)
{
  t_prog	temp;

  temp = *p1;
  *p1 = *p2;
  *p2 = temp;
}

void	sort_progs(t_prog **progs, int nbr_prog)
{
  int	index;
  int	nb;
  int	count;

  while (nbr_prog > 0)
    {
      index = 0;
      nb = progs[index]->p_num;
      count = index;
      while (index < nbr_prog)
	{
	  if (progs[index]->p_num > nb)
	    {
	      nb = progs[index]->p_num;
	      count = index;
	    }
	  index++;
	}
      if (count != nbr_prog - 1)
	my_swap(progs[count], progs[nbr_prog - 1]);
      nbr_prog--;
    }
}
