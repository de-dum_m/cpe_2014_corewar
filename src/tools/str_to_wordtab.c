/*
** str_to_wordtab.c for corewar in /home/abraha_c/coreware/cpe_2014_corewar/src/tools
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Sun Apr 13 17:48:26 2014 alexis abra
** Last update Sun Apr 13 17:48:27 2014 alexis abra
*/

#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "asm.h"
#include "tools.h"

static int	compt_word(char *str, char c, char c2)
{
  int		nb_word;
  int		i;

  if (str == NULL)
    return (0);
  i = 0;
  nb_word = 1;
  while (str[i])
    {
      if (str[i] == c || str[i] == c2)
	nb_word++;
      i++;
    }
  return (nb_word);
}

static int	cpy(char *res, char *str, int i, int k)
{
  int		c;

  c = 0;
  while (i < k)
    {
      res[c] = str[i];
      c++;
      i++;
    }
  res[c] = '\0';
  return (k + 1);
}

char		**str_to_wordtab(char *str, char c1, char c2)
{
  int		i;
  int		k;
  int		c;
  char		**tab;

  i = 0;
  k = 0;
  c = 0;
  if ((tab = malloc(sizeof(char*) * (compt_word(str, c1, c2) + 1))) == NULL)
    return (NULL);
  while (str[k])
    {
      if (str[k] == c1 || str[k] == c2)
        {
	  if ((tab[c] = malloc(sizeof(char) * (k - i) + 1)) == NULL)
	    return (NULL);
          i = cpy(tab[c++], str, i, k++);
        }
      k = k + 1;
    }
  if ((tab[c] = malloc(sizeof(char) * (k - i) + 1)) == NULL)
    return (NULL);
  cpy(tab[c++], str, i, k);
  tab[c] = NULL;
  return (tab);
}
