/*
** my_memset.c for tools in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Wed Mar 26 11:57:22 2014 de-dum_m
** Last update Sun Apr 13 11:06:13 2014 
*/

char	*my_memset(char *str, int len)
{
  int	i;

  i = 0;
  while (i < len)
      str[i++] = '\0';
  return (str);
}
