/*
** my_strcat.c for my_strcat in /home/abraha_c/rendu/Piscine-C-Jour_07/ex_01
** 
** Made by alexis abra
** Login   <abraha_c@epitech.net>
** 
** Started on  Wed Oct  9 10:06:29 2013 alexis abra
** Last update Sun Nov 24 14:18:02 2013 alexis abra
*/

int	count_char(char *str)
{
  int   i;

  i = 0;
  while (str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}

char	*my_strcat(char *dest, char *src)
{
  int	n;
  int	b;

  b = 0;
  n = count_char(dest);
  while (src[b] != '\0')
    {
      dest[n] = src[b];
      n = n + 1;
      b = b + 1;
    }
  dest[n] = '\0';
  return (dest);
}
