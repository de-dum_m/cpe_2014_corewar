/*
** my_strlen.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/tools
** 
** Made by  armita_a
** Login   <armita_a@epitech.net>
** 
** Started on  Tue Mar 11 13:45:22 2014 
** Last update Tue Mar 11 13:45:23 2014 
*/

int	my_strlen(char *str)
{
  int	index;

  index = 0;
  while (str && str[index++]);
  return (index -1);
}
