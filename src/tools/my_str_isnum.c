/*
** my_str_isnum.c for my_str_isnum in /home/auduri_a/core/cpe_2014_corewar/src/tools
** 
** Made by auduri_a
** Login   <auduri_a@epitech.net>
** 
** Started on  Fri Apr  4 13:10:00 2014 auduri_a
** Last update Sat Apr 12 17:45:24 2014 alexis abra
*/

int	my_str_isnum(char *str)
{
  int	i;
  int	compt;

  compt = 0;
  i = 0;
  while (str[i] != '\0')
    {
      if ((str[i] >= '0' && str[i] <= '9') || str[i] == '-')
        {
          compt++;
        }
      i++;
    }
  if (i == compt)
    return (1);
  else
    return (0);
}
