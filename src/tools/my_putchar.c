/*
** my_putchar.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/tools
** 
** Made by  armita_a
** Login   <armita_a@epitech.net>
** 
** Started on  Tue Mar 11 13:45:04 2014 
** Last update Tue Mar 11 13:45:06 2014 
*/

#include <unistd.h>

void	my_putchar(char c, int nb)
{
  write(nb, &c, 1);
  return ;
}
