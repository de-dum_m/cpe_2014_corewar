/*
** get_p_num.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Fri Apr 11 22:26:32 2014
** Last update Fri Apr 11 22:27:31 2014 
*/

#include "tools.h"
#include "vm.h"

int	get_p_num(t_vm *v)
{
  int	i;
  int	y;

  i = 1;
  while (i != INT_MAX)
    {
      y = 0;
      while (y < v->nbr_prog &&  y != -1)
        {
          if (v->p[y]->p_num == i)
            y = -1;
          else
            y++;
        }
      if (y ==  v->nbr_prog)
        return (i);
      i++;
    }
  return (SUCCESS);
}
