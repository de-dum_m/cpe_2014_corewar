/*
** mamelocs.c for wrf in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue Apr  8 14:25:12 2014 de-dum_m
** Last update Tue Apr  8 15:37:17 2014 de-dum_m
*/

#include <stdlib.h>
#include "tools.h"

char	*my_str_copy(char *s, int len)
{
  int	i;
  char	*res;

  i = 0;
  if (len == 0)
    len = my_strlen(s);
  else if (len < 0)
    len = my_strlen(s) - len;
  if (!(res = malloc(len + 3)))
    return (NULL);
  while (s && s[i] && i < len)
    {
      res[i] = s[i];
      i++;
    }
  res[i] = '\0';
  return (res);
}

char	*my_ucopy(char *s, int len)
{
  int	i;
  char	*res;

  i = 0;
  if (!(res = malloc(len + 3)))
    return (NULL);
  while (i < len)
    {
      res[i] = s[i];
      i++;
    }
  res[i] = '\0';
  return (res);
}
