/*
** my_put_nbr.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/tools
** 
** Made by  armita_a
** Login   <armita_a@epitech.net>
** 
** Started on  Tue Mar 11 13:45:13 2014 
** Last update Tue Mar 11 13:45:13 2014 
*/

#include "tools.h"

int	my_put_nbr(int nb)
{
  if (nb < 0)
    {
      nb = - nb;
      my_putchar('-', 1);
    }
  if (nb >= 10)
    {
      my_put_nbr(nb/10);
      my_put_nbr(nb%10);
    }
  else
    my_putchar(nb + 48, 1);
  return (0);
}
