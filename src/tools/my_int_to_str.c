/*
** my_int_to_str.c for tools in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Mar 28 14:18:25 2014 de-dum_m
** Last update Thu Apr 10 15:15:26 2014 de-dum_m
*/

#include <stdlib.h>
#include "op.h"

char	*my_int_to_str(int nb)
{
  int	i;
  int	neg;
  char	*res;

  neg = 0;
  i = REG_SIZE - 1;
  res = NULL;
  if (!(res = malloc(REG_SIZE + 2)))
    return (NULL);
  if (nb < 0)
    {
      nb *= -1;
      neg++;
    }
  while (i >= 0)
    {
      res[i--] = nb % 10 + 48;
      nb /= 10;
    }
  if (neg)
    res[0] = '-';
  res[REG_SIZE] = '\0';
  return (res);
}
