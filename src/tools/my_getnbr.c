/*
** my_getnbr.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/tools
**
** Made by  de-dum_m
** Login   <armita_a@epitech.net>
**
** Started on  Tue Mar 11 13:44:56 2014
** Last update Sun Apr 13 19:04:26 2014 
*/

#include "tools.h"
#include "op.h"

int	my_getnbr(char *str)
{
  int	index;
  int	nb;
  int	is_neg;

  nb = 0;
  index = 0;
  if (!str || !str[0])
    return (FAILURE);
  while (str && (str[index] == '-' || str[index] == '+'))
    {
      nb++;
      index++;
    }
  if (nb % 2 == 0)
    is_neg = 1;
  else
    is_neg = -1;
  nb = 0;
  while (str && str[index] >= '0' && str[index] <= '9')
    nb = (nb * 10) + str[index++] - 48;
  if (str && str[index])
    return (FAILURE);
  return (is_neg * nb);
}

int	my_getnbr_reg(char *str)
{
  int	index;
  int	nb;
  int	is_neg;

  nb = 0;
  index = 0;
  if (!str || !str[0])
    return (FAILURE);
  while (str && (str[index] == '-' || str[index] == '+'))
    {
      nb++;
      index++;
    }
  if (nb % 2 == 0)
    is_neg = 1;
  else
    is_neg = -1;
  nb = 0;
  while (str && index < REG_SIZE && str[index] >= '0' && str[index] <= '9')
    {
      nb = (nb * 10) + str[index] - 48;
      index++;
    }
   return (is_neg * nb);
}

int	my_get_chnbr(char *str, int limit)
{
  int	index;
  int	nb;
  int	is_neg;

  nb = 0;
  index = 0;
  while (str && (str[index] == '-' || str[index] == '+'))
    {
      nb++;
      index++;
    }
  if (nb % 2 == 0)
    is_neg = 1;
  else
    is_neg = -1;
  nb = 0;
  while (str && str[index] >= 0 && str[index] <= 9 && index < limit)
    {
      nb = (nb * 10) + str[index];
      index++;
    }
   return (is_neg * nb);
}
