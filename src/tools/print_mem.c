/*
** print_mem.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Apr  6 15:02:49 2014
** Last update Sun Apr 13 18:15:33 2014 de-dum_m
*/

#include "op.h"
#include "tools.h"

void	my_print_mem(char *v_mem)
{
  int	index;
  char	*str;
  int	cursor;
  int	count;
  int	val;
  int	remove_this_tmp;

  str = "0123456789ABCDEF";
  index = -1;
  while (++index < MEM_SIZE)
    {
      count = 0;
      cursor = 0;
      val = -1;
      while (++val < (remove_this_tmp = BABS(v_mem[index])))
	if (++count == 16)
	  {
	    count = 0;
	    cursor++;
	  }
      if (index % 25 == 0)
	my_printf("\n%d\t│ ", index);
      my_printf("%c%c ", str[cursor], str[count]);
    }
  my_putchar('\n', 1);
}
