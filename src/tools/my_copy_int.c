/*
** my_copy_int.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/src/tools
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Thu Apr 10 11:22:47 2014
** Last update Thu Apr 10 11:27:09 2014 
*/

#include "op.h"

void	my_copy_int(char *nb, char *str)
{
  int	i;

  i = 0;
  while (i <= REG_SIZE)
    {
      str[i] = nb[i];
      i++;
    }
}
