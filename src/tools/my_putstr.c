/*
** my_putstr.c for tools in /home/armita_a/Documents/Teck_1/System_unix/PSU_2013_minitalk/tools
** 
** Made by  armita_a
** Login   <armita_a@epitech.net>
** 
** Started on  Sun Mar  9 13:33:39 2014 
** Last update Sun Mar  9 13:33:45 2014 
*/

#include <unistd.h>
#include "tools.h"

void	my_putstr(char *str, int nb)
{
  int	index;

  index = 0;
  while (str[index])
    my_putchar(str[index++], nb);
  return ;
}
