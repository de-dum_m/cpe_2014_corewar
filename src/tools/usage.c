/*
** usage.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/tools
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue Mar 11 13:50:52 2014
** Last update Mon Mar 17 09:04:33 2014 
*/

#include <unistd.h>
#include "tools.h"

int	usage(char *str2, char *str, int fd)
{
  my_putstr("Corewar : ", fd);
  if (str2 != NULL)
    my_putstr(str2, fd);
  my_putstr(str, fd);
  my_putchar('\n', fd);
  if (fd == STDERR)
    return (FAILURE);
  return (SUCCESS);
}
