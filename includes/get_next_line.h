/*
** get_next_line.h for minishell2 in /home/de-dum_m/rendu/PSU_2013_minishell2
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  9 17:41:16 2014 de-dum_m
** Last update Sun Mar  9 17:41:16 2014 de-dum_m
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# define BUFSIZ		8

# define SUCCESS	1
# define FAILURE	-1

char	*get_next_line(int fd);

#endif /* !GET_NEXT_LINE_H_ */
