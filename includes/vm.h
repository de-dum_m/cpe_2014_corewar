/*
** vm.h for includes in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2014_corewar
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue Mar 11 13:41:39 2014 de-dum_m
** Last update Sun Apr 13 19:15:16 2014 
*/

#ifndef VM_H_
# define VM_H_

# include "op.h"

# define INT_MAX	2147483647

# define BABS(nb)	((nb) < 0) ? (256 + (nb)) : (nb)

typedef struct	s_prog
{
  int		carry;
  int		p_num;
  int		father;
  int		t_alive_nb;
  int		next_cycle;
  int		last_live;
  int		load_addr;
  int		size;
  char		reg[REG_NUMBER + 1][REG_SIZE + 1];
  char		pc[REG_SIZE + 1];
  char		*p_name;
  char		*r_name;
}		t_prog;

typedef struct	s_op
{
  int		debug;
  int		step;
}		t_op;

typedef struct	s_vm
{
  t_prog	**p;
  char		*v_mem;
  int		dump;
  int		nbr_prog;
  int		lives_done;
  int		t_alive[4];
  int		little;
  int		cycles_left;
  t_op		*tion;
}		t_vm;

/*
** check_end.c
*/
int	continue_or_not(t_vm *v);
int	init_t_alive(t_vm *v);

/*
** cor_funct/cor_getnbr.c
*/
int	cor_getnbr(char *v_mem, int size, int *pos);

/*
** cor_funct/cor_ld.c
*/
void	cor_ldi(t_vm *v, char *v_mem, int cycles, int i);
void	cor_ld(t_vm *v, char *v_mem, int cycles, int i);
void	cor_lldi(t_vm *v, char *v_mem, int cycles, int i);
void	cor_lld(t_vm *v, char *v_mem, int cycles, int i);

/*
** cor_funct/cor_op.c
*/
void	cor_add(t_vm *v, char *v_mem, int cycles, int i);
void	cor_and(t_vm *v, char *v_mem, int cycles, int i);
void	cor_or(t_vm *v, char *v_mem, int cycles, int i);
void	cor_sub(t_vm *v, char *v_mem, int cycles, int i);
void	cor_xor(t_vm *v, char *v_mem, int cycles, int i);

/*
** cor_funct/cor_other.c
*/
void	cor_aff(t_vm *v, char *v_mem, int cycles, int i);
void	cor_fork(t_vm *v, char *v_mem, int cycles, int i);
void	cor_lfork(t_vm *v, char *v_mem, int cycles, int i);
void	cor_live(t_vm *v, char *v_mem, int cycles, int i);
void	cor_zjump(t_vm *v, char *v_mem, int cycles, int i);

/*
** cor_funct/cor_st.c
*/
void	cor_sti(t_vm *v, char *v_mem, int cycles, int i);
void	cor_st(t_vm *v, char *v_mem, int cycles, int i);

/*
** cor_funct/funct_tools.c
*/
char	*get_param(t_vm *v, int i, int p_cod, int *pos);
int	add_prog(t_vm *v, int cycles, int i);
int	get_param_index(t_vm *v, int i, int p_cod, int *pos);
int	get_param_int(t_vm *v, int i, int p_cod, int *pos);
int	get_param_int_woidx(t_vm *v, int i, int p_cod, int *pos);

/*
** do_processes.c
*/
void    do_processes(t_vm *v, char *v_mem, int cycles,
		     void (*tab[16])(t_vm *, char *, int, int));
int	count_lives(t_vm *v);

/*
** free.c
*/
void	my_freezer(t_vm *v, char *v_mem);

/*
** game_on.c
*/
int	game_on(t_vm *v, char *v_mem, int cycles);

/*
** get_options.c
*/
int	put_param(int ac, char **av, t_vm *v, int prog_nb);

/*
** get_options_functions.c
*/
int	put_option_debug(t_vm *v, char **av, int *index, int *prog_nb);
int	put_option_dump(t_vm *v, char **av, int *index, int *prog_nb);
int	put_option_endian(t_vm *v, char **av, int *index, int *prog_nb);
int	put_option_step(t_vm *v, char **av, int *index, int *prog_nb);
int	put_prog_name(t_vm *v, char **av, int *index, int *prog_nb);

/*
** main.c
*/
void	init_registers(t_prog *p);

/*
** mycheck.c
*/
int	my_check(t_vm *vm);

/*
** replace_mem.c
*/
int	replace_mem(int actual_pos, int init_pos, t_prog *p, char *v_mem);

/*
** sort.c
*/
void	my_swap(t_prog *p1, t_prog *p2);
void	sort_progs(t_prog **progs, int nbr_prog);

/*
** start.c
*/
int	start_game(t_vm *v);

#endif /* !VM_H_ */
