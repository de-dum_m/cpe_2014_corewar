/*
** tools.h for includes in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/includes
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Last update Sun Apr 13 19:22:08 2014 
** Last update Sun Apr 13 11:56:47 2014 de-dum_m
*/

#ifndef TOOLS_H_
# define TOOLS_H_

# include "op.h"

# define SUCCESS	1
# define FAILURE	-1
# define STDERR		2

# define MALLOCERR	"cannot perform malloc"
# define ABS(x)		(((x) > 0) ? (x) : (-x))
# include "vm.h"

/*
** get_p_num.c
*/
int	get_p_num(t_vm *v);

/*
** my_copy_int.c
*/
void	my_copy_int(char *nb, char *str);

/*
** my_getnbr.c
*/
int	my_get_chnbr(char *str, int limit);
int	my_getnbr(char *str);
int	my_getnbr_reg(char *str);

/*
** my_int_to_str.c
*/
char	*my_int_to_str(int nb);

/*
** my_memset.c
*/
char	*my_memset(char *str, int len);

/*
** my_printf/my_count_putchar.c
*/
int	count_prints();
void	my_count_putchar(char c, int fd);
void	my_count_putstr(char *str, int fd);

/*
** my_printf/my_count_put_nbr_base.c
*/
int	my_count_putnbr_base(unsigned int nbr, char *base, int fd);
void	chop_chop_count(unsigned int nbr, int b, char *base, int fd);

/*
** my_printf/my_count_put_nbr.c
*/
int	my_count_put_nbr(int nb, int fd);

/*
** my_printf/my_printf.c
*/
char	*my_print_error_chret(const char *str, ...);
int	my_print_error(const char *str, ...);
int	my_printf(const char *str, ...);

/*
** my_printf/my_printf_tools.c
*/
void	my_put_hex(unsigned int ix, char type, int fd);
void	my_put_ptr(unsigned int ptr, int fd);
void	my_putstr_weird(char *str, int fd);

/*
** my_printf/my_put_unsigned_nbr.c
*/
int	my_put_unsigned_nbr(unsigned int nb, int fd);
void	print_modified_count(char nb, int fd);

/*
** my_putchar.c
*/
void	my_putchar(char c, int nb);

/*
** my_put_nbr.c
*/
int	my_put_nbr(int nb);

/*
** my_putstr.c
*/
void	my_putstr(char *str, int nb);

/*
** my_strcat.c
*/
char	*my_strcat(char *dest, char *src);
int	count_char(char *str);

/*
** my_strcmp.c
*/
int	my_strcmp(char *s1, char *s2);

/*
** my_strcopy.c
*/
char	*my_str_copy(char *s, int len);
char	*my_ucopy(char *s, int len);

/*
** my_str_isnum.c
*/
int	my_str_isnum(char *str);

/*
** my_strlen.c
*/
int	my_strlen(char *str);

/*
** print_mem.c
*/
void	my_print_mem(char *v_mem);

/*
** str_to_wordtab.c
*/
char	**str_to_wordtab(char *str, char c1, char c2);

/*
** usage.c
*/
int	usage(char *str2, char *str, int fd);

#endif /* !TOOLS_H_ */
