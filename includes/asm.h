/*
** asm.h for includes in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar/includes
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue Mar 25 09:49:07 2014
** Last update Sun Apr 13 19:13:31 2014 
*/

#ifndef ASM_H_
# define ASM_H_

# define SUCCESS	1
# define FAILURE	-1
# define BUFF_SIZE	512
# define PERM		0644

# include "op.h"

typedef struct  s_list
{
  char          *line;
  char          *label;
  int		num_line;
  int           nb_octet;
  int           pos_label;
  struct s_list *next;
  struct s_list *prev;
}               t_list;

typedef struct	s_coo
{
  int		num_arg;
  int		pos;
  char		**tab;
}		t_coo;

typedef struct	my_size
{
  int	taille;
  int	total;
  char	*result;
}		t_my_size;

/*
** check_arg_instr.c
*/
int		check_arg(char *str);
int		check_registre(char *str);
int		check_type(char **tab, int num_arg);
int		have_a_codage_octet(char *str);
int		have_index(char *str);

/*
** clean_instr.c
*/
char		*clean_instr(char *instr);

/*
** clean_list.c
*/
int		clean_list(t_list *root);

/*
** code_instr.c
*/
int		get_instru_code(char *code, t_list *list, t_list *root, t_coo *coo);

/*
** do_octet_code.c
*/
char		do_octet_codage(char **tab);

/*
** fill_bin.c
*/
int		fill_bin(t_list *root, char *name, int mode);

/*
** fill_header.c
*/
int		get_comment(header_t *header, t_list *root);
int		get_name(header_t *header, t_list *root);
int		get_prog_size(t_list *root);

/*
** fill_list.c
*/
int		fill_list(t_list *root, int src_fd);

/*
** get_distance_from_label.c
*/
int		get_distance_to_label(t_coo *coo, t_list *list, t_list *root);

/*
** get_nb_octet.c
*/
int		get_nb_oct(char *str);

/*
** int_to_char.c
*/
int		get_int_from_arg(char *str, t_list *list);
void		int_to_char(t_coo *coo, char *code, int nb);
void		registr_to_char(t_coo *coo, char *code, char nb);
void		shortint_to_char(t_coo *coo, char *code, short int nb);

/*
** is_label.c
*/
int		is_label(t_list *root, t_list *list, int i);

/*
** line_valide.c
*/
int		check_val_arg(char **tab, int num_instr, int nb_arg_line);
int		find_instru(char *str);
int		find_nb_arg_line(char **tab);
int		get_nb_octet(char **tab, int num_instr, int nb_arg_line);
int		line_valide(char *str, int nb_line);

/*
** list_function.c
*/
int		creat_elem_with_line(t_list *list, char *str, int i);
int		free_list(t_list*root);
t_list		*init_root(void);
void		rm_elem_list(t_list *elem);

/*
** main.c
*/
int		my_usage(void);

/*
** make_header.c
*/
header_t	*fill_header(t_list *root);

/*
** myasm.c
*/
int		my_asm(char *name, int mode);

/*
** mycheck.c
*/
int		my_check_arg(char **av, int num_arg);

/*
** my_concat.c
*/
int		my_concat(char *code, char *src, t_my_size *size);

/*
** new_name.c
*/
char		*new_name(char *name);

/*
** octet_value.c
*/
int		octet_value(char **tab, int num_instr, int num_arg);

/*
** parseur.c
*/
char		*add_label_to_list(char *str);
int		code_in_line(t_list *root, int src_fd);

/*
** rev_code.c
*/
int		rev_code(char *str, int totale, int bin_fd, int mode);

/*
** write_header.c
*/
int		write_header(header_t *header);

#endif /* !ASM_H_ */
