##
## Makefile for cpe_2014_corewar in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2014_corewar
## 
## Made by  armita_a
## Login   <armita_a@epitech.net>
## 
## Started on  Tue Mar 11 10:29:42 2014 
## Last update Sun Apr 13 19:09:21 2014 
##

VM_NAME	= corewar

VM_SRC	= src/tools/my_printf/my_count_putchar.c	\
	src/tools/my_printf/my_count_put_nbr_base.c	\
	src/tools/my_printf/my_count_put_nbr.c		\
	src/tools/my_printf/my_printf.c			\
	src/tools/my_printf/my_printf_tools.c		\
	src/tools/my_printf/my_put_unsigned_nbr.c	\
	src/tools/my_getnbr.c				\
	src/tools/my_putchar.c				\
	src/tools/my_int_to_str.c			\
	src/tools/print_mem.c				\
	src/tools/my_strcmp.c				\
	src/tools/my_put_nbr.c				\
	src/tools/my_putstr.c				\
	src/tools/my_strlen.c				\
	src/tools/get_p_num.c				\
	src/tools/usage.c				\
	src/tools/my_memset.c				\
	src/tools/my_strcopy.c				\
	src/tools/my_copy_int.c				\
	src/gnl/get_next_line.c				\
	src/VM/main.c					\
	src/VM/get_options.c				\
	src/VM/get_options_functions.c			\
	src/VM/mycheck.c				\
	src/VM/cor_funct/cor_ld.c			\
	src/VM/cor_funct/cor_getnbr.c			\
	src/VM/cor_funct/cor_st.c			\
	src/VM/cor_funct/cor_other.c			\
	src/VM/cor_funct/cor_op.c			\
	src/VM/cor_funct/funct_tools.c			\
	src/VM/game_on.c				\
	src/VM/check_end.c				\
	src/VM/do_processes.c				\
	src/VM/start.c					\
	src/VM/free.c					\
	src/VM/sort.c					\
	src/VM/replace_mem.c				\
	src/op.c

VM_OBJ	= $(VM_SRC:.c=.o)

ASM_NAME= asm

ASM_SRC	= src/op.c			\
	src/asm/main.c			\
	src/asm/mycheck.c		\
	src/asm/myasm.c			\
	src/asm/make_header.c		\
	src/asm/clean_instr.c		\
	src/asm/fill_list.c		\
	src/asm/fill_bin.c		\
	src/asm/get_nb_octet.c		\
	src/asm/parseur.c		\
	src/asm/check_arg_instr.c	\
	src/asm/line_valide.c		\
	src/asm/my_concat.c		\
	src/asm/clean_list.c		\
	src/asm/do_octet_code.c		\
	src/asm/is_label.c		\
	src/asm/fill_header.c		\
	src/asm/int_to_char.c		\
	src/asm/write_header.c		\
	src/asm/get_distance_from_label.c	\
	src/asm/code_instr.c		\
	src/asm/list_function.c \
	src/asm/octet_value.c	\
	src/asm/rev_code.c		\
	src/asm/new_name.c              \
	src/gnl/get_next_line.c		\
	src/tools/my_printf/my_count_putchar.c	\
	src/tools/my_memset.c		\
	src/tools/my_printf/my_count_put_nbr_base.c	\
	src/tools/my_printf/my_count_put_nbr.c		\
	src/tools/my_printf/my_printf.c			\
	src/tools/my_printf/my_printf_tools.c		\
	src/tools/my_printf/my_put_unsigned_nbr.c	\
	src/tools/my_strcmp.c		\
	src/tools/usage.c		\
	src/tools/my_str_isnum.c	\
	src/tools/my_strlen.c		\
	src/tools/my_putchar.c		\
	src/tools/my_putstr.c		\
	src/tools/str_to_wordtab.c	\
	src/tools/my_getnbr.c		\
	src/tools/my_strcat.c

ASM_OBJ	= $(ASM_SRC:.c=.o)

CFLAGS	= -Wall -Wextra -W -pedantic -Iincludes -g

all:	$(VM_NAME) $(ASM_NAME)

$(VM_NAME):$(VM_OBJ)
	$(CC) $(VM_OBJ) -o $(VM_NAME)
	@echo -e "[032mCompiled successfully for VM[0m"

$(ASM_NAME):$(ASM_OBJ)
	$(CC) $(ASM_OBJ) -o $(ASM_NAME)
	@echo -e "[032mCompiled successfully for ASM[0m"


clean:
	rm -f $(VM_OBJ) $(ASM_OBJ)

fclean:	clean
	rm -f $(VM_NAME) $(ASM_NAME)

re:	fclean all

.PHONY:	all clean fclean re
